<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use dektrium\user\helpers\Password,
    dektrium\user\Mailer;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property tinyinteger $count_miss_pass
 * @property boolean $must_change_pass
 * @property boolean $iaggre
 */
class User extends ActiveRecord implements IdentityInterface {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /** @inheritdoc */
    public function rules() {
        return [
            [['password_hash'], 'string', 'max' => 60],
            [['email'], 'string', 'max' => 255],
            [['count_miss_pass'], 'integer', 'max' => 10],
            [['must_change_pass', 'iaggre'], 'boolean'],
            [['email'], 'email'],
            [['count_miss_pass'], 'default', 'value' => 0],
            [['must_change_pass'], 'default', 'value' => 0],
            [['iaggre'], 'default', 'value' => 0],
        ];
    }

    /**
     * @return Mailer
     * @throws \yii\base\InvalidConfigException
     */
    protected function getMailer() {
        return \Yii::$container->get(Mailer::className());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        $result = static::findOne(['auth_key' => $token]);

        return $result;
        //throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by user email
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email) {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests() {
        return $this->hasMany(Requests::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestsManager() {
        return $this->hasMany(Requests::className(), ['manager_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile() {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * Generates a new password and sends it to the user.
     *
     * @param string $code Confirmation code.
     *
     * @return boolean
     */
    public function resendPassword() {
        $password = Password::generate(8);
        $this->setPassword($password);
        $this->save();

        $subject = 'Your temporary password';
        $message = '<p>Here is your temporary password: <br/><br/>' . $password;
        $message .= '<br/><br/>Please, <a href="https://sota.football/history" target="_blank">sign in</a> to Sota to make new permanent password</p>';

        $this->sendMessage($subject, $message);

        return true;
    }

    /**
     * Change a password and sends it to the user.
     *
     * @param string $code Confirmation code.
     *
     * @return boolean
     */
    public function changePassword(string $password) {
        $this->setPassword($password);
        $this->save();

        $subject = 'You have set a new password';
        $message = 'Your password has been successfully changed. Enjoy your Sota!';

        $this->sendMessage($subject, $message);

        return true;
    }

    // Send message for client
    final public function sendMessage(string $subject, string $message) {
        Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['systemEmail'])
                ->setTo($this->email)
                ->setSubject($subject)
                ->setHtmlBody($message)
                ->send();
    }

}
