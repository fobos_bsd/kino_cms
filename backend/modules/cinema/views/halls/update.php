<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CinemaHall */

$this->title = Yii::t('backend', 'Update Cinema Hall: {name}', [
            'name' => $model->name,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Cinema Halls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-orange">
            <div class="panel-heading"> <?= Html::encode($this->title) ?></div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'cinema' => $cinema
                ])
                ?>
            </div>
        </div>
    </div>
</div>
