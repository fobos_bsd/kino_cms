<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\caching\TagDependency,
    yii\data\Pagination;
use common\models\News;

/**
 * Description of NewsModels
 *
 * @author fobos
 */
class NewsModels extends Model {

    // Get list all data of news
    final public function getNewsList($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;
        $title = $get['title'] ?? null;
        $content = $get['content'] ?? null;
        $clan_id = $get['clan_id'] ?? null;
        $lang = $get['lang'] ?? 'ru';

        //Check is user in clan
        if (isset($clan_id) && Yii::$app->acl->isInClan($clan_id) === false) {
            $clan_id = null;
        }

        // Get data
        $contents = News::getDb()->cache(function() use ($count_per_page, $page, $title, $content, $clan_id, $lang) {
            $query = News::find()
                    ->joinWith(['newsTranslates' => function($q) use ($lang) {
                            $q->where(['news_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);
                        }])
                    ->joinWith(['author' => function($q) {
                            $q->select(['user.id', 'user.username as author']);
                        }])
                    ->orderBy(['news.created_date' => SORT_DESC, 'news.name' => SORT_ASC]);

            // Filter
            if (isset($title)) {
                $query->andWhere(['ilike', 'news_translate.title', strip_tags(trim($title))]);
            }
            if (isset($content)) {
                $query->andWhere(['ilike', 'news_translate.content', strip_tags(trim($content))]);
            }
            if (isset($clan_id)) {
                $query->andWhere(['news.clan_id' => (int) $clan_id]);
            } else {
                $query->andWhere(['is', 'news.clan_id', null]);
            }

            // Set laguage of news
            $query->andFilterWhere(['news.status_active' => true]);
            $query->andFilterWhere(['<=', 'news.publication_date', date('Y-m-d H:i:s')]);

            // Set pagination data
            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->page = $page;
            $contents = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();
            return ['contents' => $contents, 'tottal_pages' => $tottal_pages];
        }, 3600, new TagDependency(['tags' => 'cache_news_api']));

        // Set results
        $result['error'] = '';
        $result['content'] = $contents['contents'];
        $result['count_pages'] = ((int) $contents['tottal_pages'] > 0) ? ceil((int) $contents['tottal_pages'] / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Get data for one News
    final public function getNewsContent($get) {

        $news_id = (int) $get['id'];
        $lang = $get['lang'] ?? 'ru';

        // Get data
        $content = News::find()->select(['news.name', 'news_translate.content', 'news_translate.title', 'news_translate.seo_title', 'news_translate.seo_metakey', 'news_translate.seo_description', 'news.clan_id', 'user.username as author', 'news.publication_date'])
                        ->leftJoin('news_translate', 'news_translate.news_id = news.id')
                        ->leftJoin('user', '"user".id = news.author_id')
                        ->where(['news.id' => $news_id, 'news_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))])
                        ->asArray()->one();

        if (isset($content['clan_id']) && !empty($content['clan_id']) && Yii::$app->acl->isInClan($content['clan_id']) === false) {
            $content = [];
        }

        $result['error'] = '';
        $result['content'] = $content;

        return $result;
    }

}
