<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_ticket".
 *
 * @property int $id
 * @property int $cinema_hall_id
 * @property int $cinema_film_id
 * @property string $client_name
 * @property string $client_phone
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $places
 * @property string $show_time
 *
 * @property CinemaFilm $cinemaFilm
 * @property CinemaHall $cinemaHall
 */
class OrderTicket extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'order_ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['cinema_hall_id', 'cinema_film_id', 'client_name', 'created_at', 'updated_at', 'places', 'show_time'], 'required'],
            [['cinema_hall_id', 'cinema_film_id', 'created_at', 'updated_at'], 'integer'],
            [['status'], 'in', 'range' => ['Bay', 'Armor', 'In process']],
            [['client_name'], 'string', 'max' => 255],
            [['client_phone'], 'string', 'max' => 18],
            [['places'], 'string', 'max' => 1500],
            [['show_time'], 'date', 'format' => 'php:Y-m-d H:i:00'],
            [['cinema_film_id'], 'exist', 'skipOnError' => true, 'targetClass' => CinemaFilm::className(), 'targetAttribute' => ['cinema_film_id' => 'id']],
            [['cinema_hall_id'], 'exist', 'skipOnError' => true, 'targetClass' => CinemaHall::className(), 'targetAttribute' => ['cinema_hall_id' => 'id']],
            [['client_name', 'client_phone', 'status', 'places'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }],
            [['show_time'], 'filter', 'filter' => function($value) {
                    return date('Y-m-d H:i:00', strtotime($value));
                }],
            [['created_at', 'updated_at'], 'default', 'value' => time()]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'cinema_hall_id' => Yii::t('common', 'Cinema Hall ID'),
            'cinema_film_id' => Yii::t('common', 'Cinema Film ID'),
            'client_name' => Yii::t('common', 'Client Name'),
            'client_phone' => Yii::t('common', 'Client Phone'),
            'status' => Yii::t('common', 'Status'),
            'created_at' => Yii::t('common', 'Created At'),
            'places' => Yii::t('common', 'Places'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'show_time' => Yii::t('common', 'Show time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCinemaFilm() {
        return $this->hasOne(CinemaFilm::className(), ['id' => 'cinema_film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCinemaHall() {
        return $this->hasOne(CinemaHall::className(), ['id' => 'cinema_hall_id']);
    }

}
