<?php

namespace backend\modules\auth\controllers;

use dektrium\user\controllers\SecurityController as BaseSecurityControlle;

/**
 * Default controller for the `auth` module
 */
class SecurityController extends BaseSecurityControlle {
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogin() {
        $this->layout = '/login';
        $return = parent::actionLogin();
        
        return $return;
    }

}
