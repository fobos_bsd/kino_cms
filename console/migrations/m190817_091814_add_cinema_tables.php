<?php

use yii\db\Migration;

/**
 * Class m190817_091814_add_cinema_tables
 */
class m190817_091814_add_cinema_tables extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('cinema', [
            'id' => $this->primaryKey(8)->unsigned(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createTable('cinema_hall', [
            'id' => $this->primaryKey(8)->unsigned(),
            'cinema_id' => $this->integer(8)->notNull()->unsigned(),
            'name' => $this->string(255)->notNull(),
            'places' => $this->text(5000)->notNull()
        ]);

        $this->createTable('cinema_film', [
            'id' => $this->primaryKey(11)->unsigned(),
            'cinema_id' => $this->integer(8)->notNull()->unsigned(),
            'name' => $this->string(255)->notNull(),
            'show_start_date' => $this->date()->notNull(),
            'show_end_date' => $this->date()->notNull(),
            'show_times' => $this->text(1500)->notNull()
        ]);

        $this->createTable('order_ticket', [
            'id' => $this->primaryKey(11)->unsigned(),
            'cinema_hall_id' => $this->integer(8)->notNull()->unsigned(),
            'cinema_film_id' => $this->integer(11)->notNull()->unsigned(),
            'client_name' => $this->string(255)->notNull(),
            'client_phone' => $this->string(18)->null(),
            'status' => "ENUM('bay', 'armor') NOT NULL DEFAULT 'bay'",
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('fk_cinema_hall_cinema_idx', 'cinema_hall', 'cinema_id');
        $this->addForeignKey('fk_cinema_hall_cinema', 'cinema_hall', 'cinema_id', 'cinema', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('fk_cinema_film_cinema_idx', 'cinema_film', 'cinema_id');
        $this->addForeignKey('fk_cinema_film_cinema', 'cinema_film', 'cinema_id', 'cinema', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('fk_order_ticket_cinema_hall_idx', 'order_ticket', 'cinema_hall_id');
        $this->createIndex('fk_order_ticket_cinema_film_idx', 'order_ticket', 'cinema_film_id');
        $this->addForeignKey('fk_order_ticket_cinema_hall', 'order_ticket', 'cinema_hall_id', 'cinema_hall', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_order_ticket_cinema_film', 'order_ticket', 'cinema_film_id', 'cinema_film', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m190817_091814_add_cinema_tables cannot be reverted.\n";

        $this->dropForeignKey('fk_order_ticket_cinema_film', 'order_ticket');
        $this->dropForeignKey('fk_order_ticket_cinema_hall', 'order_ticket');
        $this->dropIndex('fk_order_ticket_cinema_film_idx', 'order_ticket');
        $this->dropIndex('fk_order_ticket_cinema_hall_idx', 'order_ticket');

        $this->dropForeignKey('fk_cinema_film_cinema', 'cinema_film');
        $this->dropIndex('fk_cinema_film_cinema_idx', 'cinema_film');

        $this->dropForeignKey('fk_cinema_hall_cinema', 'cinema_hall');
        $this->dropIndex('fk_cinema_hall_cinema_idx', 'cinema_hall');

        $this->dropTable('order_ticket');
        $this->dropTable('cinema_film');
        $this->dropTable('cinema_hall');
        $this->dropTable('cinema');

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190817_091814_add_cinema_tables cannot be reverted.\n";

      return false;
      }
     */
}
