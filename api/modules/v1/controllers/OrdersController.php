<?php

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\AccessControl,
    yii\web\Response,
    yii\filters\VerbFilter;
use common\models\OrderTicket;
use api\modules\v1\models\MakeArmorModels;

/**
 * Description of RepostController
 *
 * @author fobos
 */
class OrdersController extends Controller {

    public $models;

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['orders-list', 'order-item', 'make-armor', 'finish-armor', 'places-list'],
            'rules' => [
                [
                    'actions' => ['orders-list', 'order-item', 'make-armor', 'finish-armor', 'places-list'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \common\filters\CorsCustom::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Allow-Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'orders-list' => ['post', 'options'],
                'order-item' => ['get', 'options'],
                'make-armor' => ['post', 'options'],
                'finish-armor' => ['post', 'options'],
                'places-list' => ['post', 'options']
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        return $behaviors;
    }

    public function init() {
        parent::init();

        $this->models = new MakeArmorModels();
    }

    // Получение списка всех заявок с фильтром. В более боевом варианте только для менеджеров или админа
    public function actionOrdersList() {

        if (Yii::$app->request->isPost) {
            Yii::$app->response->statusCode = 200;

            $post = Yii::$app->request->post();

            // Get data with filter
            $orders = OrderTicket::find();

            if (isset($post['status'])) {
                $orders->andFilterWhere(['status' => strip_tags($post['status'])]);
            }

            if (isset($post['cinema_hall_id'])) {
                $orders->andFilterWhere(['cinema_hall_id' => (int) $post['cinema_hall_id']]);
            }

            if (isset($post['cinema_film_id'])) {
                $orders->andFilterWhere(['cinema_film_id' => (int) $post['cinema_film_id']]);
            }

            if (isset($post['show_time'])) {
                $show_time = date('Y-m-d H:i:00', strtotime($post['show_time']));

                $orders->andFilterWhere(['show_time' => $show_time]);
            }

            return [
                'error' => '',
                'content' => $orders->asArray()->all()
            ];
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }
    
    // Получение списка всех мест со статусами на определённый сеанс в определённом кинотеатре и зале.
    public function actionPlacesList() {

        if (Yii::$app->request->isPost) {
            Yii::$app->response->statusCode = 200;

            $post = Yii::$app->request->post();

           if (!isset($post['cinema_hall_id'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных зала'
                ];
            }

            if (!isset($post['cinema_film_id'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных фильма'
                ];
            }
            
            if (!isset($post['show_time'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных времени'
                ];
            }

            $result = $this->models->getArmorOrBayProgress($post);
            return $result;
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }

    // Получение отдельной заявки. В более боевом варианте только для менеджеров или админа
    public function actionOrderItem() {

        if (Yii::$app->request->isGet) {
            Yii::$app->response->statusCode = 200;

            $orderID = (int) Yii::$app->request->get('order_id');

            if (!isset($orderID) || $orderID == 0) {
                return [
                    'error' => 'No Order ID parameter',
                    'content' => ''
                ];
            }

            $order = OrderTicket::find()->where(['id' => $orderID])->asArray()->one();

            return [
                'error' => '',
                'content' => $order
            ];
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }

    // Функция бронирования или покупки билетов
    public function actionMakeArmor() {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->statusCode = 200;

            $post = Yii::$app->request->post();

            if (!isset($post['cinema_hall_id'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных зала'
                ];
            }

            if (!isset($post['cinema_film_id'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных фильма'
                ];
            }

            if (!isset($post['client_name'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных клиента'
                ];
            }

            if (!isset($post['show_time'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных времени'
                ];
            }

            if (!isset($post['places'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных о местах'
                ];
            }

            $result = $this->models->setArmorOrBayProgress($post);
            return $result;
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }
    
    // Функция завершения бронирования или покупки билетов
    public function actionFinishArmor() {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->statusCode = 200;

            $post = Yii::$app->request->post();

            if (!isset($post['order_id'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных заказа'
                ];
            }

            if (!isset($post['pay_data'])) {
                return [
                    'status' => 'error',
                    'error' => 'Нет данных оплаты'
                ];
            }

            $result = $this->models->setArmorOrBayFinish($post);
            return $result;
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }

}
