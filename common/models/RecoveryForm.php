<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;
use dektrium\user\models\RecoveryForm;

/**
 * Login form
 */
class RecoveryForm extends RecoveryForm {

    /**
     * @return array
     */
    public function rules() {
        return ArrayHelper::merge([], parent::rules());
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return ArrayHelper::merge([], parent::attributeLabels());
    }

}
