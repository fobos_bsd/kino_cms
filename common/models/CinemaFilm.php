<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cinema_film".
 *
 * @property int $id
 * @property int $cinema_id
 * @property string $name
 * @property string $show_start_date
 * @property string $show_end_date
 * @property string $show_times
 *
 * @property Cinema $cinema
 * @property OrderTicket[] $orderTickets
 */
class CinemaFilm extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'cinema_film';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['cinema_id', 'name', 'show_start_date', 'show_end_date', 'show_times'], 'required'],
            [['cinema_id'], 'integer'],
            [['show_start_date', 'show_end_date'], 'date', 'format' => 'php:Y-m-d'],
            [['show_times'], 'string', 'max' => 1500],
            [['name'], 'string', 'max' => 255],
            [['cinema_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cinema::className(), 'targetAttribute' => ['cinema_id' => 'id']],
            [['name'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }],
            [['show_start_date', 'show_end_date'], 'filter', 'filter' => function($value) {
                    return date('Y-m-d H:i:s', strtotime($value));
                }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'cinema_id' => Yii::t('common', 'Cinema ID'),
            'name' => Yii::t('common', 'Name'),
            'show_start_date' => Yii::t('common', 'Show Start Date'),
            'show_end_date' => Yii::t('common', 'Show End Date'),
            'show_times' => Yii::t('common', 'Show Times'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCinema() {
        return $this->hasOne(Cinema::className(), ['id' => 'cinema_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTickets() {
        return $this->hasMany(OrderTicket::className(), ['cinema_film_id' => 'id']);
    }

}
