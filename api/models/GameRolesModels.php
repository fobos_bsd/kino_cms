<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\caching\TagDependency,
    yii\helpers\ArrayHelper;
use common\models\RolesIngame,
    common\models\RolesIngameTranslate,
    common\models\Clan;

/**
 * Description of GameRolesModels
 *
 * @author fobos
 */
class GameRolesModels extends Model {

    // Get data for GameRoles
    final public function getRolesData($get) {

        $lang = $get['lang'] ?? 'ru';

        // Get data
        $contents = RolesIngame::getDb()->cache(function() use ($lang, $get) {
            $query = RolesIngame::find()->select(['roles_ingame.*', 'roles_ingame_translate.name as role_name'])
                    ->leftJoin('roles_ingame_translate', 'roles_ingame_translate.roles_ingame_id = roles_ingame.id')
                    ->leftJoin('games_roles', 'games_roles.roles_ingame_id = roles_ingame.id')
                    ->where(['roles_ingame_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);

            if (isset($get['game_id']) && !empty($get['game_id'])) {
                $query->andFilterWhere(['games_roles.games_id' => $get['game_id']]);
            }

            if (isset($get['clan_id']) && !empty($get['clan_id'])) {
                $game_id = (int) ArrayHelper::getValue(Clan::find()->where(['id' => $get['clan_id']])->asArray()->one(), 'game_id');
                $query->andFilterWhere(['games_roles.games_id' => $game_id]);
            }

            return $query->asArray()->all();
        }, 7200, new TagDependency(['tags' => 'cache_games_roles_api']));

        $result['error'] = '';
        $result['content'] = $contents;

        return $result;
    }

    // Get data for GameRoles
    final public function getRolesTranslate($get) {

        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = ArrayHelper::map(RolesIngameTranslate::find()->where(['language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))])->asArray()->all(), 'roles_ingame_id', 'name');

        $i = 0;
        foreach ($query as $key => $val) {
            $data[$i]['role_id'] = $key;
            $data[$i]['name'] = $val;
            $i++;
        }

        $result['error'] = '';
        $result['content'] = $data;

        return $result;
    }

}
