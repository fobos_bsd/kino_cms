<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\data\Pagination;
use common\models\ClanUsers,
    common\models\User;

/**
 * Description of GroupUsersModels
 *
 * @author fobos
 */
class GroupUsersModels extends Model {

    // Get list all data
    final public function getUserGroupsList($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;
        $clan_id = $get['clan_id'] ?? null;
        $user_id = $get['user_id'] ?? null;
        $clan_role_id = $get['role_id'] ?? null;
        $active_status = $get['active_status'] ?? null;
        $user_status = $get['user_status'] ?? null;
        $start_date = $get['start_date'] ?? null;
        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = ClanUsers::find()
                ->select(['clan_users.*',
                    'clan_group.id as group_id',
                    'clan_group.name as group_name',
                    'clan_users_roles.mane_role',
                    'roles_ingame.name as role_name',
                    'roles_ingame.icons as role_icon',
                    'roles_ingame_translate.name as role_title',
                    'clan_group.boss_id as group_boss_id',
                    'clan_group.color as clan_group_color',
                    'user.username as nick',
                    'clan.game_id as game_id'
                ])
                ->leftJoin('clan_group', 'clan_group.id = clan_users.clan_group_id')
                ->leftJoin('clan_users_roles', 'clan_users_roles.clan_users_id = clan_users.id')
                ->leftJoin('roles_ingame', 'clan_users_roles.clan_roles_id = roles_ingame.id')
                ->leftJoin('roles_ingame_translate', 'roles_ingame_translate.roles_ingame_id = roles_ingame.id')
                ->leftJoin('clan', 'clan.id = clan_users.clan_id')
                ->leftJoin('"user"', 'clan_users.user_id = "user".id')
                ->orderBy(['clan_users.user_status' => SORT_ASC, 'clan_users.active_status' => SORT_DESC, 'clan_users_roles.mane_role' => SORT_DESC]);

        $query->where(['roles_ingame_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);

        // Filter
        if (isset($active_status)) {
            $query->andFilterWhere(['clan_users.active_status' => strip_tags(trim($active_status))]);
        }
        if (isset($user_status)) {
            $query->andFilterWhere(['clan_users.user_status' => strip_tags(trim($user_status))]);
        }
        if (isset($start_date)) {
            $query->andFilterWhere(['between', 'clan_users.start_date', date('Y-m-d 00:00:00', strtotime($start_date)), date('Y-m-d 23:59:59', strtotime($start_date))]);
        }
        if (isset($clan_id)) {
            $query->andFilterWhere(['clan_users.clan_id' => (int) $clan_id]);
        }
        if (isset($user_id)) {
            $query->andFilterWhere(['clan_users.user_id' => (int) $user_id]);
        }
        if (isset($clan_role_id)) {
            $query->andFilterWhere(['clan_roles.id' => (int) $clan_role_id]);
        }

        // Set pagination data
        $countQuery = clone $query;
        $tottal_pages = (int) $countQuery->count();
        $pages = new Pagination(['totalCount' => $tottal_pages]);
        $pages->defaultPageSize = $count_per_page;
        $pages->page = $page;
        $contents = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->asArray()
                ->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $contents;
        $result['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Get data for one User Group
    final public function getUserGroupData($get) {

        $itemID = (int) $get['id'];
        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = ClanUsers::find()->select(['clan_users.*'])
                ->joinWith(['user' => function($sq) {
                        $sq->select(['user.username', 'user.id as user_id', 'user.email']);
                    }])
                ->joinWith(['clanUsersRoles' => function($sq)use($lang) {
                        $sq->select(['roles_ingame.name', 'roles_ingame.id', 'roles_ingame.icons', 'clan_users_roles.mane_role', 'clan_users_roles.clan_users_id', 'roles_ingame_translate.name as title', 'user_games.nickname', 'user_games.level']);
                        $sq->leftJoin('roles_ingame', 'clan_users_roles.clan_roles_id = roles_ingame.id');
                        $sq->leftJoin('roles_ingame_translate', 'roles_ingame_translate.roles_ingame_id = roles_ingame.id');
                        $sq->leftJoin('user_games', 'user_games.roles_ingame_id = roles_ingame.id');
                        $sq->where(['roles_ingame_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);
                    }])
                ->joinWith(['clanStatusList' => function($sq) {
                        $sq->select(['clan_status_list.name', 'clan_status_list.id as status_id']);
                    }])
                ->joinWith(['clanGroup' => function($sq) {
                        $sq->select(['clan_group.name', 'clan_group.id as group_id', 'clan_group.color', 'clan_group.boss_id']);
                    }])
                ->joinWith(['clan' => function($sq) {
                        $sq->select(['clan.name', 'clan.id as group_id', 'clan.active', 'clan.game_id as game_id', 'clan.description', 'games.name as game_name']);
                        $sq->leftJoin('games', 'games.id = clan.game_id');
                    }])
                ->where(['clan_users.id' => $itemID]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

    // Get list users not in group
    final public function getUserListNoIngroup($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;

        // Get data
        $query = User::find()->select(['user.id', 'user.username', 'user.email'])
                ->joinWith(['profile' => function($sq) {
                        $sq->select(['profile.user_id', 'profile.name', 'profile.avatar']);
                    }])
                ->joinWith('clanUsers', false)
                ->where(['clan_users.active_status' => 'no confirmed'])
                ->andWhere(['is', 'user.blocked_at', null])
                ->andWhere(['is', 'clan_users.who_confirmed', null])
                ->andWhere(['clan_users.clan_id' => $get['id']])
                ->orderBy(['user.confirmed_at' => SORT_DESC])
                ->groupBy(['user.id']);

        // Set pagination data
        $countQuery = clone $query;
        $tottal_pages = (int) $countQuery->count();
        $pages = new Pagination(['totalCount' => $tottal_pages]);
        $pages->defaultPageSize = $count_per_page;
        $pages->page = $page;
        $contents = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->asArray()
                ->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $contents;
        $result['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Add user to clan
    final public function addUserToClan($put) {
        $result['error'] = '';
        $result['status'] = 'success';

        $model = new ClanUsers();
        $model->loadDefaultValues();
        $model->clan_id = (int) $put['clan_id'];
        $model->user_id = (int) $put['user_id'];
        $model->active_status = $put['active_status'];
        $model->user_status = $put['user_status'];
        $model->start_date = $model->modification_date = date('Y-m-d');
        $model->confirmed_key = md5($model->user_id . $model->start_date . 'GfhjkzYtn324') ?? null;
        $model->parent_user_id = $put['parent_user_id'] ?? null;
        $model->clan_group_id = $put['clan_group_id'] ?? null;

        if (!$model->save()) {
            Yii::$app->response->statusCode = 500;
            $result['status'] = 'error';
            $result['error'] = (array) $model->getErrors();
        }

        return $result;
    }

}
