<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\helpers\ArrayHelper,
    yii\web\UploadedFile,
    yii\caching\TagDependency,
    yii\db\ArrayExpression,
    yii\data\Pagination;
use common\models\User,
    common\models\UserGames,
    common\models\Profile,
    common\models\ClanUserRulesRelations,
    common\models\Timezones,
    common\models\Clan,
    common\models\ClanUsers,
    common\models\ClanUsersRoles,
    common\models\UserFreands,
    common\models\Games,
    common\models\SocialAccount;

/**
 * Description of UsersModels
 *
 * @author fobos
 */
class UsersModels extends Model {

    // Get list all data
    final public function getUserList($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;
        $ban = $get['ban'] ?? null;
        $username = $get['username'] ?? null;
        $email = $get['email'] ?? null;

        // Get data
        $contents = User::getDb()->cache(function() use ($count_per_page, $page, $ban, $username, $email) {
            $query = User::find()
                    ->select(['id', 'username', 'email', 'registration_ip', 'last_login_at'])
                    ->orderBy(['created_at' => SORT_DESC, 'username' => SORT_ASC]);

            // Filter
            if (isset($ban)) {
                $query->where(['is not', 'blocked_at', null]);
            }
            if (isset($username)) {
                $query->andFilterWhere(['like', 'username', strip_tags(trim($username))]);
            }
            if (isset($email)) {
                $query->andFilterWhere(['like', 'email', strip_tags(trim($email))]);
            }

            $query->andFilterWhere(['is not', 'confirmed_at', null]);

            // Set pagination data
            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->page = $page;
            $contents = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();


            return ['contents' => $contents, 'tottal_pages' => $tottal_pages];
        }, 3600, new TagDependency(['tags' => 'cache_filter_partners']));

        // Set results
        $result['error'] = '';
        $result['content'] = $contents['contents'];
        $result['count_pages'] = ((int) $contents['tottal_pages'] > 0) ? ceil((int) $contents['tottal_pages'] / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Get list of partners for game
    final public function getPartnerList($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = $get['count_par_pare'] ?? 20;
        $game = $get['game'] ?? null;
        $gender = $get['gender'] ?? null;
        $level = $get['levels'] ?? null;
        $game_type = $get['game_types'] ?? null;
        $game_genres = $get['game_genres'] ?? null;
        $experience = $get['experience'] ?? null;
        $timezone = $get['timezone'] ?? null;
        $game_time = $get['game_time'] ?? null;
        $role = $get['role'] ?? null;
        $contry = $get['contry'] ?? null;
        $lang = $get['lang'] ?? null;
        $nikname = $get['nikname'] ?? null;

        //Get time_shift for user
        if (isset($timezone)) {
            $time_shift = ArrayHelper::getValue(Timezones::find()->where(['id' => $timezone])->asArray()->one(), 'time_shift');
        } else {
            $time_shift = ArrayHelper::getValue(Profile::find()->select(['timezones.time_shift as time_shift'])->leftJoin('timezones', 'timezones.id = profile.timezone')->where(['user_id' => Yii::$app->user->id])->asArray()->all(), 'time_shift');
        }

        if (!isset($time_shift))
            $time_shift = '+00';

        // Get data with cache in redis
        $contents = User::getDb()->cache(function() use ($count_per_page, $page, $game, $gender, $level, $game_type, $game_genres, $experience, $timezone, $game_time, $role, $contry, $lang, $nikname, $time_shift) {
            $query = User::find()
                    ->select(['user.id', 'user.username'])
                    ->joinWith(['userGames' => function($q) use($level, $role, $experience, $game_time, $game_type, $game, $time_shift, $lang, $game_genres) {
                            $q->select(['user_games.*']);

                            // Filter
                            if (isset($level[0]) && isset($level[1])) {
                                $q->andFilterWhere(['and', ['>=', 'user_games.level', $level[0]], ['<=', 'user_games.level', $level[1]]]);
                            }
                            if (isset($role)) {
                                $q->andFilterWhere(['user_games.roles_ingame_id' => $role]);
                            }
                            if (isset($experience)) {
                                $q->andFilterWhere(['user_games.experience' => $experience]);
                            }
                            if (isset($game_time[0]) && isset($game_time[1])) {
                                $q->andFilterWhere(['or',
                                    ['between', 'user_games."game_time"[1][1]', $game_time[0] . $time_shift, $game_time[1] . $time_shift],
                                    ['between', 'user_games."game_time"[1][2]', $game_time[0] . $time_shift, $game_time[1] . $time_shift],
                                    ['between', 'user_games."game_time"[2][1]', $game_time[0] . $time_shift, $game_time[1] . $time_shift],
                                    ['between', 'user_games."game_time"[2][2]', $game_time[0] . $time_shift, $game_time[1] . $time_shift],
                                    ['between', 'user_games."game_time"[3][1]', $game_time[0] . $time_shift, $game_time[1] . $time_shift],
                                    ['between', 'user_games."game_time"[3][2]', $game_time[0] . $time_shift, $game_time[1] . $time_shift],
                                ]);
                            }
                            if (isset($game_type) && is_array($game_type) && count($game_type) > 0) {
                                foreach ($game_type as $gitem) :
                                    $q->andFilterWhere(['user_games.game_type_id' => $gitem]);
                                endforeach;
                            }

                            $q->joinWith(['games' => function($q) use ($game, $game_genres) {
                                    $q->select(['games.id', 'games.name']);

                                    if (isset($game)) {
                                        $q->andFilterWhere(['games.id' => $game]);
                                    }
                                    $q->joinWith(['gameType' => function($sq) {
                                            $sq->select(['game_type.id', 'game_type.name']);
                                        }]);
                                    $q->joinWith(['gameGenre' => function($sq) use ($game_genres) {
                                            $sq->select(['game_genre.id', 'game_genre.name']);
                                            if (isset($game_genres) && is_array($game_genres) && count($game_genres) > 0) {
                                                foreach ($game_genres as $item) :
                                                    $sq->andFilterWhere(['game_genre.id' => $item]);
                                                endforeach;
                                            }
                                        }]);
                                }]);
                        }])
                    ->joinWith(['profile' => function($sq) use ($gender, $contry, $lang) {
                            $sq->select(['profile.gender', 'profile.avatar', 'profile.user_id']);
                            if (isset($gender)) {
                                $sq->andFilterWhere(['profile.gender' => $gender]);
                            }
                            if (isset($contry)) {
                                $sq->andFilterWhere(['profile.contry_id' => $contry]);
                            }
                            if (isset($lang)) {
                                $sq->andFilterWhere(['profile.language_id' => $lang]);
                            }
                        }])
                    ->joinWith(['clanUsers' => function($sq) {
                    $sq->select(['clan_users.user_id', 'clan_users.id', 'clan_users.clan_id']);
                    //$sq->andWhere(['clan_users.active_status' => 'active']);
                    $sq->joinWith(['clan' => function($ssq) {
                            $ssq->select(['clan.id', 'clan.name']);
                            //$ssq->andFilterWhere(['clan.active' => true]);
                        }]);
                }]);

            // Filter

            if (isset($nikname)) {
                $query->andFilterWhere(['ilike', 'user.username', $nikname]);
            }

            $query->andFilterWhere(['is not', 'user.confirmed_at', null]);
            $query->groupBy(['"user".id', 'user_games.level', 'user_games.experience'])->addOrderBy(['user_games.level' => SORT_DESC, 'user_games.experience' => SORT_DESC]);

            // Set pagination data
            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->setPageSize($count_per_page);
            $pages->pageSizeLimit = [1, 50];
            $pages->page = $page;
            $contents = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();

            return ['contents' => $contents, 'tottal_pages' => $tottal_pages];
        }, 3600, new TagDependency(['tags' => 'cache_filter_partners']));

        // Set results
        $result['error'] = '';
        $result['content'] = $contents['contents'];
        $result['count_pages'] = ((int) $contents['tottal_pages'] > 0) ? ceil((int) $contents['tottal_pages'] / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Get data for one User
    final public function getUserData($get) {

        $itemID = (int) Yii::$app->user->id;
        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = User::find()->select(['user.username',
                    'user.email',
                    'user.id',
                    'profile.name as fio',
                    'profile.public_email',
                    'profile.bio',
                    'profile.phone',
                    'profile.avatar',
                    'profile.timezone',
                    'profile.surname',
                    'profile.middle_name',
                    'profile.city',
                    'profile.timezone',
                    'profile.gender',
                    'profile.birthday',
                    'profile.contry_id',
                ])
                ->leftJoin('profile', '"profile"."user_id"="user"."id"')
                ->with(['socialAccount' => function($q) {
                        $q->select(['social_account.id', 'social_account.user_id', 'social_account.provider']);
                    }])
                ->where(['user.id' => $itemID]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

    // Get public data for one User
    final public function getUserPablicData($get) {

        $itemID = (int) $get['id'];
        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = User::find()->select(['user.username',
                    'user.id',
                    'profile.public_email',
                    'profile.avatar'
                ])
                ->leftJoin('profile', '"profile"."user_id"="user"."id"')
                ->joinWith(['clanUsers' => function($q)use($lang) {
                        $q->select(['clan_users.id', 'clan_users.active_status', 'clan_users.user_status', 'clan_users.start_date', 'clan_users.user_id', 'clan_users.clan_id', 'clan.name as clan_name', 'clan.game_id', 'clan_users_roles.mane_role', 'roles_ingame.name', 'roles_ingame.id role_id', 'roles_ingame.icons', 'clan_users_roles.mane_role', 'clan_users_roles.clan_users_id', 'roles_ingame_translate.name as title', 'user_games.nickname', 'user_games.level']);
                        $q->leftJoin('clan', 'clan.id = clan_users.clan_id');
                        $q->andFilterWhere(['clan.active' => true]);
                        $q->leftJoin('clan_users_roles', 'clan_users_roles.clan_users_id = clan_users.id');
                        $q->leftJoin('roles_ingame', 'clan_users_roles.clan_roles_id = roles_ingame.id');
                        $q->leftJoin('user_games', 'user_games.roles_ingame_id = roles_ingame.id');
                    }])
                ->where(['user.id' => $itemID]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

    // Get User games
    final public function getUserGames() {
        $userID = Yii::$app->user->id;

        // Get data
        $query = Games::find()
                ->leftJoin('user_games', 'user_games.games_id = games.id')
                ->with('gameGenre')
                ->with('gameCategories')
                ->with('gameType')
                ->with(['userGames' => function($q) {
                        $q->select(['user_games.user_id', 'user_games.id', 'user_games.games_id', 'user_games.level', 'user_games.how_long_play', 'user_games.experience', 'user_games.roles_ingame_id', 'user_games.nickname', 'user_games.game_type_id', 'array_to_json(game_time) as game_time']);
                        $q->where(['user_games.user_id' => Yii::$app->user->id]);
                    }])
                ->where(['user_games.user_id' => Yii::$app->user->id]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Get clans where this user send request to in on
    final public function getRequestClan() {
        $userID = Yii::$app->user->id;

        // Get data
        $query = User::find()
                ->joinWith(['clanUsers' => function($q) {
                        $q->select(['clan_users.user_id', 'clan_users.clan_id', 'clan_users.clan_role_id']);
                        $q->joinWith(['clan' => function($sq) {
                                $sq->select(['clan.id', 'clan.name', 'clan.logo', 'clan.description']);
                            }]);
                    }])
                ->where(['user.id' => $userID, 'clan_users.who_invited' => null, 'clan_users.active_status' => 'no confirmed']);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Get clans where this user has Invite from clan`s admin
    final public function getInvitedClan() {
        $userID = Yii::$app->user->id;

        // Get data
        $query = User::find()
                        ->joinWith(['clanUsers' => function($q) {
                                $q->select(['clan_users.user_id', 'clan_users.clan_id', 'clan_users.clan_role_id']);
                                $q->joinWith(['clan' => function($sq) {
                                        $sq->select(['clan.id', 'clan.name', 'clan.logo', 'clan.description']);
                                    }]);
                            }])
                        ->where(['user.id' => $userID, 'clan_users.active_status' => 'no confirmed'])->andWhere(['is not', 'clan_users.who_invited', null]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Update user info data
    final public function updateProfile($data) {
        $userID = Yii::$app->user->id;

        // Set results
        $result['error'] = '';
        $result['status'] = 'success';

        if (($model = Profile::findOne(['user_id' => $userID])) !== null) {
            $model->scenario = 'api';
            $model->name = $data['name'] ?? $model->name;
            $model->surname = $data['surname'] ?? $model->surname;
            $model->middle_name = $data['middle_name'] ?? $model->middle_name;
            $model->birthday = $data['birthday'] ?? $model->birthday;
            $model->contry_id = $data['contry'] ?? $model->contry_id;
            $model->city = $data['city'] ?? $model->city;
            $model->gender = $data['gender'] ?? $model->gender;
            $model->timezone = $data['timezone'] ?? $model->timezone;
            $model->phone = $data['phone'] ?? $model->phone;
            $model->save();
        } else {
            $result['error'] = 'User with this ID was not found';
            $result['status'] = 'error';
        }

        return $result;
    }

    // Update user avatar
    final public function updateAvatar($avatar) {
        $userID = Yii::$app->user->id;

        // Set results
        $result['error'] = '';
        $result['status'] = 'success';

        if (($model = Profile::findOne(['user_id' => $userID])) !== null) {
            $model->scenario = 'update';
            $model->avatar = UploadedFile::getInstanceByName('avatar');
            $model->avatar = $model->upload();
            if (!$model->save()) {
                $result['error'] = (array) $model->getErrors();
                $result['status'] = 'error';
            }
        } else {
            $result['error'] = 'User with this ID was not found';
            $result['status'] = 'error';
        }

        return $result;
    }

    // Delete user from site
    final public function deleteProfile() {
        $userID = Yii::$app->user->id;

        // Set results
        $result['error'] = '';
        $result['content'] = '';
        $result['status'] = 'success';

        if (($model = User::findOne(['id' => $userID])) !== null) {
            $model->scenario = 'api';
            $model->updated_at = $model->blocked_at = time();
            $model->save();
            $result['content'] = 'Profile`s was remove from site success';
        } else {
            $result['error'] = 'User with this ID was not found';
            $result['status'] = 'error';
        }

        return $result;
    }

    // Update user password
    final public function updatePassword($data) {
        $userID = Yii::$app->user->id;

        // Set results
        $result['error'] = '';
        $result['content'] = '';
        $result['status'] = 'success';

        if (!isset($data['password']) || !isset($data['retrypassword']) || strlen($data['password']) < 6 || $data['password'] !== $data['retrypassword']) {
            $result['error'] = 'Wrong Password or shoter then 6 char';
            $result['content'] = '';
            $result['status'] = 'error';
        } else {
            if (($model = User::findOne(['id' => $userID])) !== null) {
                $model->scenario = 'api';
                $model->password_hash = Yii::$app->security->generatePasswordHash($data['password'], Yii::$app->getModule('user')->cost);
                $model->updated_at = time();
                $model->save();
                $result['content'] = 'Profile`s data was changed success';
            } else {
                $result['error'] = 'User with this ID was not found';
                $result['status'] = 'error';
            }
        }

        return $result;
    }

    // Get user data in one game
    final public function getUserRolesInGame($data) {
        $userID = (int) $data['user_id'];
        $gameID = (int) $data['game_id'];

        // Get data
        $query = UserGames::find()
                ->joinWith('rolesIngame')
                ->where(['user_games.user_id' => $userID, 'user_games.games_id' => $gameID]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Add user rule data
    final public function setUserRule($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        $model = new ClanUserRulesRelations();
        $model->clan_user_rules_id = $data['clan_rules_id'];
        $model->clan_users_id = $data['clan_users_id'];
        $model->rules_value = $data['rules_value'];
        if (!$model->save()) {
            $result['status'] = 'error';
            $result['error'] = (array) $model->getErrors();
        }

        return $result;
    }

    // Get user friends
    final public function getUserFriends($data) {
        $userID = (int) $data['user_id'];

        // Get data
        $query = UserFreands::find()
                ->joinWith(['friend' => function($q) {
                        $q->select(['user.id', 'user.username']);
                        $q->joinWith(['profile' => function($sq) {
                                $sq->select(['profile.user_id', 'profile.name', 'profile.surname', 'profile.middle_name', 'profile.avatar']);
                            }]);
                    }])
                ->where(['user_freands.user_id' => $userID, 'user_freands.active_status' => true, 'user_freands.ban' => false]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Get user request to friends
    final public function getRequestFriends() {

        // Get data
        $query = UserFreands::find()
                ->joinWith(['friend' => function($q) {
                        $q->select(['user.id', 'user.username']);
                        $q->joinWith(['profile' => function($sq) {
                                $sq->select(['profile.user_id', 'profile.name', 'profile.surname', 'profile.middle_name', 'profile.avatar']);
                            }]);
                    }])
                ->where(['user_freands.user_id' => Yii::$app->user->id, 'user_freands.active_status' => false, 'user_freands.ban' => false]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Add user friend
    final public function addUserFriend($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        if (UserFreands::findOne(['user_id' => $data['friend_id'], 'freand_id' => Yii::$app->user->id]) === null) {
            $model = new UserFreands();
            $model->user_id = $data['friend_id'];
            $model->freand_id = Yii::$app->user->id;
            $model->active_status = false;
            $model->ban = false;
            $model->created_at = date('Y-m-d');
            $model->save();
        }

        return $result;
    }

    // Add user friend to ban
    final public function addFriendToBan($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        if (($model = UserFreands::findOne(['user_id' => Yii::$app->user->id, 'freand_id' => $data['friend_id'], 'ban' => false])) !== null) {
            $model->ban = true;
            $model->save();
        }

        return $result;
    }

    // Confirm user friend request
    final public function confirmFriendRequest($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        if (($model = UserFreands::findOne(['user_id' => Yii::$app->user->id, 'freand_id' => $data['friend_id'], 'ban' => false, 'active_status' => false])) !== null) {
            $model->active_status = true;
            $model->save();
        }

        return $result;
    }

    // Delete user friend
    final public function deleteFriend($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        if (($model = UserFreands::findOne(['user_id' => Yii::$app->user->id, 'freand_id' => $data['friend_id']])) !== null) {
            $model->delete();
        }

        return $result;
    }

    // Add user game data
    final public function setUserGame($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        if (($model = UserGames::findOne(['user_id' => Yii::$app->user->id, 'games_id' => $data['game_id'], 'nickname' => $data['nickname']])) === null) {
            $model = new UserGames();
            $model->user_id = Yii::$app->user->id;
            $model->games_id = $data['game_id'];
            $model->nickname = $data['nickname'];
            $model->level = $data['level'] ?? 0;
            $model->how_long_play = $data['how_long_play'] ?? 0;
            $model->experience = $data['experience'] ?? 0;
            $model->game_time = $data['game_time'] ?? null;
            $model->roles_ingame_id = $data['roles_ingame_id'] ?? null;
            if (!$model->save()) {
                $result['status'] = 'error';
                $result['error'] = (array) $model->getErrors();
            }
        } else {
            $model->level = $data['level'] ?? $model->level;
            $model->how_long_play = $data['how_long_play'] ?? $model->how_long_play;
            $model->experience = $data['experience'] ?? $model->experience;
            $model->game_time = $data['game_time'] ?? $model->game_time;
            $model->roles_ingame_id = $data['roles_ingame_id'] ?? $model->roles_ingame_id;
            if (!$model->save()) {
                $result['status'] = 'error';
                $result['error'] = (array) $model->getErrors();
            }
        }

        return $result;
    }

    // Delete user`s game
    final public function deleteUserGame($data) {
        $result['status'] = 'success';
        $result['error'] = '';

        $count_person_ingame = $this->countPersonInGame($data['game_id']);

        // Check is it one item for the game and is oner for clan
        if ($count_person_ingame < 2 && $this->isUserOnerClanByGame($data['game_id']) > 0) {
            $result['status'] = 'error';
            $result['error'] = 'You are have active clan where you oner';
            return $result;
        }

        $user_clans_id = $this->getUserClansIDByGame($data['game_id']);
        // Remove user from all clan for this game
        if ($count_person_ingame == 1) {
            foreach ($user_clans_id as $item) {
                ClanUsers::deleteAll(['id' => $item]);
            }

            // Delete user out of the game
            UserGames::deleteAll(['user_id' => Yii::$app->user->id, 'games_id' => $data['game_id']]);

            return $result;
        }

        if (($model = UserGames::findOne(['user_id' => Yii::$app->user->id, 'games_id' => $data['game_id'], 'nickname' => $data['nickname']])) !== null) {
            $user_clans_id = $this->getUserClansIDByGame($data['game_id']);

            // Remove clan user roles
            foreach ($user_clans_id as $item) {
                ClanUsersRoles::deleteAll(['clan_users_id' => $item, 'clan_roles_id' => $model->roles_ingame_id]);
            }

            // Delete user`s person from the game
            $model->delete();
        }

        return $result;
    }

    // Delete user social acount
    final public function deleteSocialnk($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        if (($model = SocialAccount::findOne(['user_id' => Yii::$app->user->id, 'id' => $data['id']])) !== null) {
            $model->delete();
        }

        return $result;
    }

    // Get users clans
    final public function getClans() {
        $content = Clan::find()->select(['clan.id', 'clan.name', 'clan.game_id', 'clan.description', 'clan.logo', 'clan.oner_id'])
                        ->leftJoin('clan_users', 'clan_users.clan_id = clan.id')
                        ->where(['clan.active' => true, 'clan_users.user_id' => Yii::$app->user->id])->orWhere(['clan.oner_id' => Yii::$app->user->id])
                        ->groupBy(['clan.id'])
                        ->asArray()->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $content;

        return $result;
    }

    // Get ID user`s clan
    final private function getUserClansIDByGame(int $game_id) {
        return ArrayHelper::getColumn(ClanUsers::find()->select(['clan_users.id as id'])->leftJoin('clan', 'clan.id = clan_users.clan_id')->where(['clan_users.user_id' => Yii::$app->user->id, 'clan.game_id' => $game_id])->asArray()->all(), 'id');
    }

    // Get count personaz in game
    final private function countPersonInGame(int $game_id) {
        return UserGames::find()->where(['user_id' => Yii::$app->user->id, 'games_id' => $game_id])->count();
    }

    // Is user oner clan
    final private function isUserOnerClanByGame(int $game_id) {
        return Clan::find()->where(['oner_id' => Yii::$app->user->id, 'game_id' => $game_id, 'active' => true])->count();
    }

}
