<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model;
use common\models\Contry;

/**
 * Description of ContriesModels
 *
 * @author fobos
 */
class ContriesModels extends Model {

    // Get data for Contries
    final public function getContriesData($get) {

        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = Contry::find()->select(['contry.id', 'contry_translate.name as contry_name'])
                ->leftJoin('contry_translate', 'contry_translate.contry_id = contry.id')
                ->where(['contry_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

}
