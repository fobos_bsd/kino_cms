<?php

use yii\bootstrap4\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\CinemaHall */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col">
            <?=
            $form->field($model, 'cinema_id')->widget(Select2::className(), [
                'data' => $cinema,
                'options' => ['multiple' => false, 'placeholder' => Yii::t('backend', 'Select cinema')],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'places')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
