<?php

namespace api\modules\v1\controllers;

use Yii,
    yii\rest\Controller,
    yii\filters\AccessControl,
    yii\filters\auth\HttpBearerAuth,
    yii\web\Response,
    yii\filters\VerbFilter;
use common\models\LoginForm;
use dektrium\user\models\RegistrationForm;

/**
 * Description of RepostController
 *
 * @author fobos
 */
class IndexController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();

        // Filter actions
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['login', 'error', 'logout', 'register'],
            'rules' => [
                [
                    'actions' => ['login', 'error', 'register'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => ['logout'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \common\filters\CorsCustom::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Allow-Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'PUT', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => false,
            ],
        ];

        // Filter actions
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'login' => ['post', 'options'],
                'logout' => ['post', 'options'],
                'register' => ['post', 'options'],
            ]
        ];

        // Format data
        $behaviors['ContentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => ['application/json' => Response::FORMAT_JSON]
        ];

        unset($behaviors['authenticator']);
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['logout']
        ];

        return $behaviors;
    }

    // User logout
    public function actionLogin() {

        if (Yii::$app->request->isPost) {
            Yii::$app->response->statusCode = 200;

            if (Yii::$app->user->isGuest) {

                $post = Yii::$app->request->post();

                $model = new LoginForm();
                $model->email = (isset($post['email']) && !empty($post['email'])) ? strip_tags(trim($post['email'])) : '';
                $model->password = (isset($post['password']) && !empty($post['password'])) ? strip_tags(trim($post['password'])) : '';

                if ($model->login()) {

                    return [
                        'error' => '',
                        'auth_token' => Yii::$app->user->identity->auth_key,
                        'user_id' => Yii::$app->user->id,
                        'user_name' => Yii::$app->user->identity->username,
                        'groups' => Yii::$app->authManager->getRolesByUser(Yii::$app->user->id),
                    ];
                } else {
                    return [
                        'error' => 'Incorrect E-mail or password: ',
                        'auth_token' => '',
                        'groups' => ['guest'],
                        'user_id' => null,
                        'user_name' => null,
                    ];
                }
            } else {

                return [
                    'error' => '',
                    'auth_token' => Yii::$app->user->identity->auth_key,
                    'groups' => Yii::$app->authManager->getRolesByUser(Yii::$app->user->id),
                    'user_id' => Yii::$app->user->id,
                    'user_name' => Yii::$app->user->identity->username,
                ];
            }

            return [
                'error' => 'System error authorisation',
                'auth_token' => '',
                'groups' => ['guest'],
                'courier_id' => null,
                'user_id' => null,
                'user_name' => null,
            ];
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }

    // User logout
    public function actionLogout() {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->statusCode = 200;

            // Add to log
            if (!Yii::$app->user->isGuest) {
                Yii::$app->getUser()->logout();
            }

            return [
                'error' => '',
                'auth_token' => '',
                'groups' => ['guest'],
                'user_id' => null
            ];
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }

    public function actionRegister() {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            Yii::$app->response->statusCode = 200;

            if (!isset($post['email']) || empty(trim(strip_tags($post['email'])))) {
                return [
                    'error' => 'No email parameter',
                    'status' => 'erorr',
                ];
            }

            if (!isset($post['username']) && isset($post['email'])) {
                preg_match('/^(.*)@/', $post['email'], $matches);
                $post['username'] = substr($matches[0], 0, -1) . '_' . date('is');
            }

            if (!isset($post['password']) || empty(trim(strip_tags($post['password'])))) {
                $post['password'] = $this->GenPassword(8);
            };

            /** @var RegistrationForm $model */
            $model = Yii::createObject(RegistrationForm::className());

            $model->username = $post['username'];
            $model->email = $post['email'];
            $model->password = $post['password'];

            if ($model->register()) {
                return [
                    'error' => '',
                    'status' => 'success',
                ];
            } else {
                return [
                    'error' => (array) $model->getErrors(),
                    'status' => 'erorr',
                ];
            }
        } else {
            Yii::$app->response->statusCode = 404;
            return ['error' => 'Not Found'];
        }
    }

    private function GenPassword($length = 10) {
        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $length = intval($length);
        $size = strlen($chars) - 1;
        $password = "";
        while ($length--)
            $password .= $chars[rand(0, $size)];
        return $password;
    }

}
