<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\data\Pagination;
use common\models\ChatMessage,
    common\models\ClanUsers;

/**
 * Description of MessagesModels
 *
 * @author fobos
 */
class MessagesModels extends Model {

    // Get list all data
    final public function getMessagesList($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;
        $clan_id = $get['clan_id'] ?? null;
        $author_id = $get['author_id'] ?? null;
        $system = $get['no_system'] ?? 0;
        $whom_id = $get['whom_id'] ?? null;
        $created_at = $get['created'] ?? null;
        $message = $get['message'] ?? null;
        $is_readed = $get['is_readed'] ?? null;
        
        
        //Check is user in the clan. If is not then set not exist clan ID
        if(ClanUsers::findOne(['clan_id' => $clan_id, 'user_id' => Yii::$app->user->id]) === null) {
                    $clan_id = null;
        }

        // Get data
        $query = ChatMessage::find()
                ->with(['author' => function($q) {
                        $q->select(['user.id', 'user.username', 'profile.avatar as avatar']);
                        $q->leftJoin('profile', 'profile.user_id = "user".id');
                    }])
                ->orderBy(['created_at' => SORT_ASC]);

        // Filter
        if (isset($clan_id)) {
            $query->andFilterWhere(['clan_id' => (int) $clan_id]);
        }
        if (isset($author_id)) {
            $query->andFilterWhere(['author_id' => (int) $author_id]);
        }
        if (isset($is_readed)) {
            $query->andFilterWhere(['is_readed' => (int) $is_readed]);
        }
        if ($system == 1) {
            $query->andFilterWhere(['$ne', 'author_id', (int) $system]);
        }
        if (isset($whom_id)) {
            $query->andFilterWhere(['whom_id' => (int) $whom_id]);
        }
        else {
            $query->andFilterWhere(['whom_id' => '']);
        }
        if (isset($created_at)) {
            $query->andFilterWhere(['$qte', 'created_at', strip_tags(trim($created_at))]);
        }
        if (isset($message)) {
            $query->andFilterWhere(['message' => strip_tags(trim($message))]);
        }

        // Set pagination data
        $countQuery = clone $query;
        $tottal_pages = (int) $countQuery->count();
        $pages = new Pagination(['totalCount' => $tottal_pages]);
        $pages->defaultPageSize = $count_per_page;
        $pages->page = $page;
        $contents = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->asArray()
                ->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $contents;
        $result['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Get data for one Message
    final public function getMessageData($get) {

        $itemID = strip_tags(trim($get['id']));

        // Get data
        $query = ChatMessage::find()
                ->with(['clan' => function($sq) {
                        $sq->select(['clan.name', 'clan.active', 'clan.game_id as game_id', 'clan.description']);
                    }])
                ->with(['author' => function($sq) {
                        $sq->select(['user.username as author_nick', 'user.email as author_email']);
                    }])
                ->where(['_id' => $itemID]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

}
