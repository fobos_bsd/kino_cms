<?php

use yii\bootstrap4\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\search\CinemaFilmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col no-padding-right no-padding-left">
    <div class="panel panel-yellow panel-wrapper-yellow">
        <div class="panel-heading"> <?= Yii::t('backend', 'Filter') ?>
            <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-angle-down"></i></a> </div>
        </div>
        <div class="panel-wrapper collapse" aria-expanded="true">
            <div class="panel-body">

                <?php
                $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                            'options' => [
                                'data-pjax' => 1
                            ],
                ]);
                ?>

                <div class="row">
                    <div class="col no-padding-left">
                        <?= $form->field($model, 'id') ?>
                    </div>
                    <div class="col">
                        <?=
                        $form->field($model, 'cinema_id')->widget(Select2::className(), [
                            'data' => $cinema,
                            'options' => ['multiple' => false, 'placeholder' => Yii::t('backend', 'Select cinema')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])
                        ?>
                    </div>
                    <div class="col no-padding-right">
                        <?= $form->field($model, 'name') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col no-padding-left">
                        <?=
                        $form->field($model, 'show_start_date')->widget(DatePicker::className(), [
                            'options' => ['placeholder' => 'Укажите дату начала'],
                            'pickerButton' => ['icon' => 'time'],
                            'convertFormat' => true,
                            'language' => 'ru',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'php:Y-m-d',
                                'todayHighlight' => false,
                                'startDate' => date('Y-m-d')
                            ]
                        ])
                        ?>
                    </div>
                    <div class="col no-padding-right">
                        <?=
                        $form->field($model, 'show_end_date')->widget(DatePicker::className(), [
                            'options' => ['placeholder' => 'Укажите дату окончания'],
                            'pickerButton' => ['icon' => 'time'],
                            'convertFormat' => true,
                            'language' => 'ru',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'php:Y-m-d',
                                'todayHighlight' => false,
                                'startDate' => date('Y-m-d')
                            ]
                        ])
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-info']) ?>
                    <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-secondary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
