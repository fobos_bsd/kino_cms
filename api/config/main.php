<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@api/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/index',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST login' => 'login',
                        'OPTIONS login' => 'login',
                        'POST logout' => 'logout',
                        'POST register' => 'register',
                        'OPTIONS register' => 'register',
                        'OPTIONS logout' => 'logout',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/orders',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST orders-list' => 'orders-list',
                        'OPTIONS orders-list' => 'orders-list',
                        'GET order-item' => 'order-item',
                        'OPTIONS order-item' => 'order-item',
                        'POST make-armor' => 'make-armor',
                        'OPTIONS make-armor' => 'make-armor',
                        'POST finish-armor' => 'finish-armor',
                        'OPTIONS finish-armor' => 'finish-armor',
                        'POST places-list' => 'places-list',
                        'OPTIONS places-list' => 'places-list'
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
