<?php

use yii\bootstrap4\Html,
    yii\grid\GridView,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CinemaFilmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Cinema Films');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row white-box no-margin no-padding">
    <div class="col-12">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>
            <?= Html::a(Yii::t('backend', 'Add'), ['create'], ['class' => 'btn btn-info']) ?>
        </p>
        <?php echo $this->render('_search', ['model' => $searchModel, 'cinema' => $cinema]); ?>
    </div>
    <?php Pjax::begin(['enablePushState' => false, 'options' => ['class' => 'col-12']]); ?>
    <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table dataTable table-striped table-bordered color-bordered-table info-bordered-table'
            ],
            'columns' => [
                'id',
                ['attribute' => 'cinema_id',
                    'content' => function($data) {
                        return $data->cinema->name;
                    }
                ],
                'name',
                ['attribute' => 'show_start_date',
                    'content' => function($data) {
                        return Yii::$app->formatter->asDate($data->show_start_date);
                    }
                ],
                ['attribute' => 'show_end_date',
                    'content' => function($data) {
                        return Yii::$app->formatter->asDate($data->show_end_date);
                    }
                ],
                'show_times:ntext',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => Yii::t('backend', 'Действия'),
                    'headerOptions' => ['width' => '60'],
                    'template' => '<div class="action-buttons">{update}&nbsp;{delete}</div>',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<i class="ace-icon fa fa-edit fa-lg"></i>', $url, ['class' => 'green']);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="ace-icon fa fa-times fa-lg"></i>', $url, ['id' => 'id-btn-dialog2', 'class' => 'red', 'data' => [
                                            'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                            ]]);
                        },
                    ],
                ],
            ],
        ]);
        ?>
    </div>
    <?php Pjax::end(); ?>
</div>
