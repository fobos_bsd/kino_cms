<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model;
use common\models\Pages;

/**
 * Description of PageModels
 *
 * @author fobos
 */
class PagesModels extends Model {

    // Get data for one Page
    final public function getPageContent($get) {

        $alias = strip_tags(trim($get['alias']));
        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = Pages::find()->select(['pages.*', 'pages_translate.content', 'pages_translate.title', 'pages_translate.seo_title', 'pages_translate.seo_metakey', 'pages_translate.seo_description'])
                ->leftJoin('pages_translate', 'pages_translate.pages_id = pages.id')
                ->where(['pages.alias' => $alias, 'pages_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang))), 'pages.status_active' => true]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

}
