<?php

namespace backend\modules\auth\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl;
use backend\modules\auth\models\HistoryLog;

/**
 * HistoryController implements the CRUD actions for JobHistory model.
 */
class HistoryController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logs'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    /**
     * List of logs
     */
    public function actionLogs() {
        $obj_history = new HistoryLog();
        $files_list = $obj_history->getListLog();

        return $this->render('logs', ['files' => $files_list]);
    }

}
