<?php

namespace common\components;

use Yii,
    yii\base\Component,
    yii\helpers\ArrayHelper,
    common\models\AuthAssignment,
    common\models\AuthItem;

class UserData extends Component {

    public function init() {
        parent::init();
    }

    public function getUserGroups() {
        return ArrayHelper::getColumn(AuthItem::find()->joinWith('authAssignments', true)->where(['auth_assignment.user_id' => Yii::$app->user->id])->asArray()->all(), 'id');
    }

    public function isGurier() {
        if (AuthAssignment::find()->where(['in', 'item_name', ['courier']])->andWhere(['user_id' => Yii::$app->user->id])->count() !== '0') {
            return true;
        } else {
            return false;
        }
    }

    public function isAdmin() {
        if (AuthAssignment::find()->where(['in', 'item_name', ['admin']])->andWhere(['user_id' => Yii::$app->user->id])->count() !== '0') {
            return true;
        } else {
            return false;
        }
    }

}
