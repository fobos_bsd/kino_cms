<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\caching\TagDependency;
use common\models\GameCategoriesTranslate;

/**
 * Description of GameCategoriesModels
 *
 * @author fobos
 */
class GameCategoriesModels extends Model {

    // Get data for GameCategoriesTranslate
    final public function getTranslate($get) {

        $lang = $get['lang'] ?? 'ru';

        // Get data with cache in redis
        $content = GameCategoriesTranslate::getDb()->cache(function() use ($lang) {
            $query = GameCategoriesTranslate::find()->select(['game_categories_id', 'title', 'description'])->where(['language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))])->asArray()->all();
            return $query;
        }, 3600, new TagDependency(['tags' => 'cache_gamecategory_translate']));

        $result['error'] = '';
        $result['content'] = $content;

        return $result;
    }

}
