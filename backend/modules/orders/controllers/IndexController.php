<?php

namespace backend\modules\orders\controllers;

use Yii,
    yii\web\Controller,
    yii\web\NotFoundHttpException,
    yii\filters\VerbFilter,
    yii\filters\AccessControl;
use common\models\OrderTicket,
    common\models\search\OrderTicketSearch,
    common\models\CinemaFilm,
    common\models\CinemaHall;

/**
 * IndexController implements the CRUD actions for OrderTicket model.
 */
class IndexController extends Controller {

    public $halls, $films;

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init() {
        parent::init();

        // Get halls
        $arr_halls = CinemaHall::find()->select(['cinema_hall.id as id', 'cinema_hall.name as hall_name', 'cinema.name as cinema'])->rightJoin('cinema', 'cinema.id = cinema_hall.cinema_id')->asArray()->all();
        foreach ($arr_halls as $item) {
            $halls[$item['id']] = $item['hall_name'] . ' - ' . $item['cinema'];
        }

        // Get films
        $arr_films = CinemaFilm::find()->select(['cinema_film.id as id', 'cinema_film.name as film_name', 'cinema.name as cinema'])->rightJoin('cinema', 'cinema.id = cinema_film.cinema_id')->asArray()->all();
        foreach ($arr_films as $item) {
            $films[$item['id']] = $item['film_name'] . ' - ' . $item['cinema'];
        }

        $this->halls = $halls;
        $this->films = $films;
    }

    /**
     * Lists all OrderTicket models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OrderTicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'halls' => $this->halls,
                    'films' => $this->films
        ]);
    }

    /**
     * Displays a single OrderTicket model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrderTicket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new OrderTicket();
        $model->loadDefaultValues();
        $model->created_at = $model->updated_at = time();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
                    'model' => $model,
                    'halls' => $this->halls,
                    'films' => $this->films
        ]);
    }

    /**
     * Updates an existing OrderTicket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->updated_at = time();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
                    'model' => $model,
                    'halls' => $this->halls,
                    'films' => $this->films
        ]);
    }

    /**
     * Deletes an existing OrderTicket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrderTicket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderTicket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = OrderTicket::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
    }

}
