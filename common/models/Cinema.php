<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cinema".
 *
 * @property int $id
 * @property string $name
 *
 * @property CinemaFilm[] $cinemaFilms
 * @property CinemaHall[] $cinemaHalls
 */
class Cinema extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'cinema';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCinemaFilms() {
        return $this->hasMany(CinemaFilm::className(), ['cinema_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCinemaHalls() {
        return $this->hasMany(CinemaHall::className(), ['cinema_id' => 'id']);
    }

}
