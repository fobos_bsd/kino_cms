<?php

use yii\db\Migration;

/**
 * Class m190817_074847_add_root_user
 */
class m190817_074847_add_root_user extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insert('user', [
            'username' => 'root',
            'email' => 'poluostrov-krym@meta.ua',
            'password_hash' => '$2y$12$b3dLPBSq7z.Lx03.naa9BOF798oGZ37gzGzYFpcGo4u7o4SglgfYW',
            'auth_key' => 'TqCa9ep0pViqCsABLj4tu9fijiE0cbnC',
            'confirmed_at' => '1484686908',
            'registration_ip' => '194.135.17.20',
            'created_at' => '1484686908',
            'updated_at' => '1557068847',
            'flags' => 0
        ]);

        $this->insert('profile', [
            'user_id' => 1,
            'name' => 'Artem',
            'gravatar_id' => 'd41d8cd98f00b204e9800998ecf8427e'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m190817_074847_add_root_user cannot be reverted.\n";
        $this->delete('profile', ['user_id' => 1]);
        $this->delete('user', ['id' => 1]);
        
        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190817_074847_add_root_user cannot be reverted.\n";

      return false;
      }
     */
}
