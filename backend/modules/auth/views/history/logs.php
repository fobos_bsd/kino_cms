<?php

use yii\bootstrap\Html;

$this->title = Yii::t('backend', 'Логи авторизаций');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row white-box no-margin no-padding">
    <div class="col-12">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-12">
        <?php
        if (isset($files)) {
            foreach ($files as $file) :
                ?>
                <div class="col-3">
                    <p class="text-center m-b-15 m-t-20">
                        <a href="/uploads/auth_history/<?= $file ?>" download="<?= $file ?>" class="btn btn-danger btn-block btn-outline" style="cursor: pointer;"><?= $file ?></a>
                    </p>
                </div>
                <?php
            endforeach;
        }
        ?>
    </div>
</div>