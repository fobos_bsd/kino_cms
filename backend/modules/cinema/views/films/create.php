<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CinemaFilm */

$this->title = Yii::t('backend', 'Create Cinema Film');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Cinema Films'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-orange">
            <div class="panel-heading"> <?= Html::encode($this->title) ?></div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'cinema' => $cinema
                ])
                ?>
            </div>
        </div>
    </div>
</div>
