<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model;
use common\models\ClanGroup;

/**
 * Description of GroupsModels
 *
 * @author fobos
 */
class ClanGroupsModels extends Model {

    // Get list all data
    final public function getList($get) {

        // Set start data
        $name = $get['name'] ?? null;
        $desc = $get['desc'] ?? null;

        // Get data
        $query = ClanGroup::find()
                ->orderBy(['name' => SORT_ASC, 'parent_id' => SORT_DESC]);

        // Filter
        if (isset($name)) {
            $query->andWhere(['like', 'name', strip_tags(trim($name))]);
        }
        if (isset($desc)) {
            $query->andWhere(['like', 'description', strip_tags(trim($desc))]);
        }
        if (isset($get['clan_id'])) {
            $query->andWhere(['clans_id' => $get['clan_id']]);
        }

        $contents = $query->asArray()
                ->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $contents;

        return $result;
    }

    // Get data for one Group
    final public function getData($get) {

        $itemID = (int) $get['id'];

        // Get data
        $query = ClanGroup::find()
                ->joinWith(['boss' => function($q) {
                        $q->select(['user.id', 'user.username', 'user.email', 'profile.avatar as avatar']);
                        $q->leftJoin('profile', '"profile"."user_id" = "user"."id"');
                    }])
                ->where(['clan_group.id' => $itemID]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

}
