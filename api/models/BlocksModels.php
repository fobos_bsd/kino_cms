<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model;
use common\models\CodeBlock;

/**
 * Description of EventsModels
 *
 * @author fobos
 */
class BlocksModels extends Model {

    // Get data for one Block
    final public function getBlockContent($get) {
        $lang = $get['lang'] ?? 'ru';

        $block_name = strip_tags(trim($get['block_name']));

        // Get data
        $query = CodeBlock::find()->select(['code_block.name', 'code_block_translate.content'])
                ->leftJoin('code_block_translate', 'code_block_translate.code_block_id = code_block.id')
                ->where(['code_block.name' => $block_name, 'code_block_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

}
