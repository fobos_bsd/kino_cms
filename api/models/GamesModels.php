<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\caching\TagDependency;
use common\models\Games;

/**
 * Description of GamesModels
 *
 * @author fobos
 */
class GamesModels extends Model {

    // Get data for one Game
    final public function getGameData($get) {

        // Get data
        $query = Games::find()->with('gameType')->with('gameGenre')
                ->where(['games.id' => $get['id']]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

    // Get list of data for Games
    final public function getListData($get) {

        $lang = $get['lang'] ?? 'ru';

        // Get data
        $contents = Games::getDb()->cache(function() use ($lang) {
            $query = Games::find()->joinWith(['roles' => function($q) use ($lang) {
                            $q->joinWith(['rolesIngameTranslates' => function($sq) use ($lang) {
                                    $sq->select(['roles_ingame_translate.roles_ingame_id', 'roles_ingame_translate.name']);
                                    $sq->where(['roles_ingame_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);
                                }])
                            ;
                        }])->with('gameType')->with('gameGenre')->with('gameCategories')->asArray()->all();
            return $query;
        }, 7200, new TagDependency(['tags' => 'cache_games_api']));

        $result['error'] = '';
        $result['content'] = $contents;

        return $result;
    }

}
