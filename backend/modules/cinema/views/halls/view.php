<?php

use yii\bootstrap4\Html,
    yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CinemaHall */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Cinema Halls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"> <?= Html::encode($this->title) ?></div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body panel-view">

                    <p>
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                        <?=
                        Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </p>

                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            ['attribute' => 'cinema_id',
                                'value' => function($model) {
                                    return $model->cinema->name;
                                }
                            ],
                            'name',
                            'places:ntext',
                        ],
                    ])
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
