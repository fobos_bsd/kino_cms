<?php

use yii\db\Migration;

/**
 * Class m190818_205016_add_time_to_order
 */
class m190818_205016_add_time_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_ticket', 'show_time', $this->dateTime()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190818_205016_add_time_to_order cannot be reverted.\n";
        $this->dropColumn('order_ticket', 'show_time');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190818_205016_add_time_to_order cannot be reverted.\n";

        return false;
    }
    */
}
