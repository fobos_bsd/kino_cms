<?php

namespace common\filters;

use Yii;
use yii\filters\Cors;

/**
 * Description of CorsCustom
 *
 * @author fobos
 */
class CorsCustom extends Cors {
    public function beforeAction($action)
    {
        parent::beforeAction($action);

        if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
            Yii::$app->response->statusCode = 200;
            Yii::$app->end();
        }

        return true;
    }
}
