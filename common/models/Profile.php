<?php

namespace common\models;

use Yii,
    yii\web\UploadedFile;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $public_email
 * @property string $gravatar_email
 * @property string $gravatar_id
 * @property string $location
 * @property string $website
 * @property string $bio
 * @property string $timezone
 * @property string $avatar
 * @property string $work_place
 *
 * @property User $user
 */
class Profile extends \dektrium\user\models\Profile {

    const SCENARIO_PROFILE = 'profile';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['bio'], 'string'],
            [['name', 'public_email', 'gravatar_email', 'location', 'website', 'class', 'ethnicity'], 'string', 'max' => 255],
            [['work_place'], 'string', 'max' => 500],
            [['gravatar_id'], 'string', 'max' => 32],
            [['timezone'], 'string', 'max' => 40],
            [['gender'], 'in', 'range' => ['male', 'female', '0', '1']],
            [['class', 'ethnicity', 'gender'], 'required', 'on' => self::SCENARIO_PROFILE],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['avatar'], 'file', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024],
            [['avatar'], 'safe', 'on' => 'update'],
        ];
    }

    /*
     * @return array
     */

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PROFILE] = ['class', 'ethnicity', 'gender'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'public_email' => 'Public Email',
            'gravatar_email' => 'Gravatar Email',
            'gravatar_id' => 'Gravatar ID',
            'location' => 'Location',
            'website' => 'Website',
            'bio' => 'Bio',
            'timezone' => 'Timezone',
            'gender' => 'Gender',
            'class' => 'Class',
            'ethnicity' => 'Ethnicity',
            'avatar' => 'Avatar',
            'work_place' => 'Work Place'
        ];
    }

    /*
     * @return validate
     */

    public function beforeSave($insert) {

        if (isset($this->gender)) {
            switch ($this->gender) {
                case '0': $this->gender = 'male';
                    break;
                case '1': $this->gender = 'female';
                    break;
            }
        }

        if (isset($this->class)) {
            switch ($this->class) {
                case '0': $this->class = 'sophomore English class';
                    break;
                case '1': $this->class = 'junior English class';
                    break;
            }
        }

        if (isset($this->ethnicity)) {
            switch ($this->ethnicity) {
                case '0': $this->ethnicity = 'White';
                    break;
                case '1': $this->ethnicity = 'Hispanic of Latino';
                    break;
                case '2': $this->ethnicity = 'Back or African American';
                    break;
                case '3': $this->ethnicity = 'Native American/American Indian';
                    break;
                case '4': $this->ethnicity = 'Asian/Pacific Islander';
                    break;
                case '5': $this->ethnicity = 'Other';
                    break;
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function upload() {
            $dir = Yii::getAlias('@frontend') . '/web/uploads/avatars';
            if (!file_exists($dir)) {
                mkdir($dir, 0775, true);
            }

            $path = $this->avatar->baseName . '.' . $this->avatar->extension;
            $this->avatar->saveAs($dir . '/' . $path);
            return $path;
    }

}
