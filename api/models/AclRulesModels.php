<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model;
use common\models\ClanStatusRules,
    common\models\ClanUserRules;

/**
 * Description of AclRulesModels
 *
 * @author fobos
 */
class AclRulesModels extends Model {

    // Get list all data for roles
    final public function getRoleRules($get) {

        // Set start data
        $name = $get['name'] ?? null;
        $desc = $get['desc'] ?? null;

        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = ClanStatusRules::find()
                ->select(['clan_status_rules.*', 'clan_status_rules_translate.title as title'])
                ->orderBy(['name' => SORT_ASC]);

        // Filter
        $query->where(['clan_status_rules.active_status' => true]);

        if (isset($name)) {
            $query->andWhere(['like', 'clan_status_rules.name', strip_tags(trim($name))]);
        }
        if (isset($desc)) {
            $query->andWhere(['like', 'clan_status_rules.description', strip_tags(trim($desc))]);
        }

        $query->andWhere(['clan_status_rules_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);

        $query->leftJoin('clan_status_rules_translate', 'clan_status_rules_translate.clan_status_rules_id = clan_status_rules.id');

        $contents = $query->asArray()->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $contents;

        return $result;
    }

    // Get list all data for users
    final public function getUserRules($get) {

        // Set start data
        $name = $get['name'] ?? null;
        $desc = $get['desc'] ?? null;

        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = ClanUserRules::find()
                ->select(['clan_user_rules.*', 'clan_user_rules_relations.title as title'])
                ->orderBy(['name' => SORT_ASC]);

        // Filter
        $query->where(['clan_user_rules.active_status' => true]);

        if (isset($name)) {
            $query->andWhere(['like', 'clan_user_rules.name', strip_tags(trim($name))]);
        }
        if (isset($desc)) {
            $query->andWhere(['like', 'clan_user_rules.description', strip_tags(trim($desc))]);
        }

        $query->andWhere(['clan_user_rules_relations.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);

        $query->leftJoin('clan_user_rules_relations', 'clan_user_rules_relations.clan_user_rules_id = clan_user_rules.id');

        $contents = $query->asArray()->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $contents;

        return $result;
    }

}
