<?php

namespace backend\modules\auth\models;

use Yii,
    yii\helpers\ArrayHelper;
use dektrium\user\models\Profile as BaseProfile;

/**
 * Description of Profile
 *
 * @author fobos
 */
class Profile extends BaseProfile {

    /**
     * @inheritdoc
     */
    public function rules() {
        return ArrayHelper::merge([
                    [['phone', 'work_phone'], 'string', 'max' => 45],
                    [['add_number'], 'string', 'max' => 15],
                    [['avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'gif'], 'mimeTypes' => ['image/png', 'image/jpeg', 'image/gif'], 'maxFiles' => 1, 'maxSize' => 4194104, 'on' => 'create'],
                    [['avatar'], 'safe', 'on' => 'update'],
                        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return ArrayHelper::merge([
                    'phone' => 'Phone',
                    'work_phone' => 'Work Phone',
                    'add_number' => 'Add code',
                    'avatar' => 'Avatar'
                        ], parent::attributeLabels());
    }

}
