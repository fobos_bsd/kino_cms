<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\modules\v1\models;

use Yii,
    yii\base\Model,
    yii\helpers\Json;
use common\models\OrderTicket,
    common\models\CinemaHall;

/**
 * Description of NotifyModels
 *
 * @author fobos
 */
class MakeArmorModels extends Model {

    // Бронирование или покупка билетов
    final public function setArmorOrBayProgress(array $data): array {

        // Получаем и устанавливаем более безопасные данные для добавления в БД
        $cinema_hall_id = (int) $data['cinema_hall_id'];
        $cinema_film_id = (int) $data['cinema_film_id'];
        $show_time = date('Y-m-d H:i:00', strtotime($data['show_time']));

        // Занято ли хоть одно из запрашиваемых мест
        $is_buise = $this->hasBuisyPlace($data['places'], $cinema_hall_id, $cinema_film_id, $show_time);

        if ($is_buise === true) {
            $result['status'] = 'error';
            $result['error'] = 'К сожалению Вы не успели и места заняты или указали их не правильно';

            return $result;
        }

        // Ставим статус в процессе, если места свободны и переходим к оплате
        $model = new OrderTicket();
        $model->cinema_hall_id = $cinema_hall_id;
        $model->cinema_film_id = $cinema_film_id;
        $model->client_name = $data['client_name'];
        $model->client_phone = $data['client_phone'] ?? null;
        $model->status = 'In process';
        $model->created_at = $model->updated_at = time();
        $model->places = Json::encode($data['places']);
        $model->show_time = $show_time;
        $model->save();

        // Get save ID
        $orderID = $model->id;

        // Set results
        $result['error'] = '';
        $result['status'] = 'success';
        $result['order_id'] = $orderID;

        return $result;
    }

    // Завершение Бронирования или покупка билетов
    final public function setArmorOrBayFinish(array $data): array {

        // Получаем и устанавливаем более безопасные данные для добавления в БД
        $order_id = (int) $data['order_id'];
        $pay_data = strip_tags(trim($data['pay_data']));

        $model = OrderTicket::findOne(['id' => $order_id]);
        $model->status = $pay_data;
        $model->save();

        // Set results
        $result['error'] = '';
        $result['status'] = 'success';

        return $result;
    }
    
    // Получение массива мест в зале со статусами занятости на определённый сеанс
    final public function getArmorOrBayProgress(array $data): array {

        // Получаем и устанавливаем более безопасные данные для добавления в БД
        $cinema_hall_id = (int) $data['cinema_hall_id'];
        $cinema_film_id = (int) $data['cinema_film_id'];
        $show_time = date('Y-m-d H:i:00', strtotime($data['show_time']));

        // Получаем массив занятых мест
        $buise_places = $this->getBuisePlacesWithStatus($cinema_hall_id, $cinema_film_id, $show_time);
        
        // Получение массива существующих
        $existed_places = $this->getCinemaPlaces($cinema_hall_id);
        
        // Собираем массив всех мест и ставим по умолчанию свободными
        $arr_places_st = [];
        foreach ($existed_places as $item) {
            $arr_places_st[$item] = 'Free';
        }
        
        // Замена дефолтніх значений занятости мет на значения из перечня занятіх
        $content = array_replace($arr_places_st, $buise_places);

        // Set results
        $result['error'] = '';
        $result['content'] = $content;

        return $result;
    }

    // Функция для проверки заняты ли места. Если используется не только тут, но глобально - выносим в компоненты
    private function hasBuisyPlace($places, int $cinema_hall_id, int $cinema_film_id, string $show_time): bool {
        $result = false;

        // Форматированный массив из запроса
        $arr_places = $this->parsePlaces($places);

        // Получаем массив занятых мест
        $buise_places = $this->getBuisePlaces($cinema_hall_id, $cinema_film_id, $show_time);
        
        // Получение массива существующих
        $existed_places = $this->getCinemaPlaces($cinema_hall_id);

        // Если есть совпадения, то вернём истину (занято). При необходимости можно возвращать и какие именно места заняты
        if (count(array_intersect($buise_places, $arr_places)) > 0 || count(array_diff($arr_places, $existed_places)) > 0) {
            $result = true;
        }

        return $result;
    }

    // Функция получения массива мест на сеанс в кинотеатр уже занятых
    private function getBuisePlaces(int $cinema_hall_id, int $cinema_film_id, string $show_time): array {
        $result = [];
        $arr_places = OrderTicket::find()->select(['places'])->where(['cinema_hall_id' => $cinema_hall_id, 'cinema_film_id' => $cinema_film_id, 'show_time' => $show_time])->asArray()->all();
        foreach ($arr_places as $json_place) {
            $item = Json::decode($json_place['places']);
            $result = $this->parsePlaces($item);
        }

        return $result;
    }
    
    // Функция получения массива мест на сеанс в кинотеатр занятых со статусами
    private function getBuisePlacesWithStatus(int $cinema_hall_id, int $cinema_film_id, string $show_time): array {
        $result = [];
        $arr_places = OrderTicket::find()->select(['places', 'status'])->where(['cinema_hall_id' => $cinema_hall_id, 'cinema_film_id' => $cinema_film_id, 'show_time' => $show_time])->asArray()->all();
        foreach ($arr_places as $json_place) {
            $item = Json::decode($json_place['places']);
            $arr_places = $this->parsePlaces($item);
            
            foreach ($arr_places as $item_r) {
                $result[$item_r] = $json_place['status'];
            }
        }

        return $result;
    }

    // Функция получения массива мест в кинотеатре
    private function getCinemaPlaces(int $cinema_hall_id): array {
        $result = [];
        $arr_places = CinemaHall::find()->select(['places'])->where(['id' => $cinema_hall_id])->asArray()->all();
        foreach ($arr_places as $json_place) {
            $item = Json::decode($json_place['places']);
            $result = $this->parsePlaces($item);
        }

        return $result;
    }

    // Парсер массива мест и приведения к массиву вида "ряд/место"
    private function parsePlaces(array $places) {
        $result = [];

        foreach ($places as $key => $val) {
            foreach ($val as $item) {
                array_push($result, $key . '/' . $item);
            }
        }

        return $result;
    }

}
