<?php

use yii\db\Migration;

/**
 * Class m190817_081111_add_admin_group
 */
class m190817_081111_add_admin_group extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insert('auth_item', [
            'name' => 'admin',
            'type' => 1,
            'created_at' => '1535749604',
            'updated_at' => '1535749604'
        ]);

        $this->insert('auth_assignment', [
            'item_name' => 'admin',
            'user_id' => 1,
            'created_at' => '1535749682'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m190817_081111_add_admin_group cannot be reverted.\n";
        $this->delete('auth_assignmente', ['user_id' => 1]);
        $this->delete('auth_item', ['name' => 'admin', 'type' => 1]);

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190817_081111_add_admin_group cannot be reverted.\n";

      return false;
      }
     */
}
