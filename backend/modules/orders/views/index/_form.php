<?php

use yii\bootstrap4\Html,
    yii\widgets\ActiveForm;
use kartik\widgets\Select2,
    kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\OrderTicket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'client_phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?=
            $form->field($model, 'cinema_hall_id')->widget(Select2::className(), [
                'data' => $halls,
                'options' => ['multiple' => false, 'placeholder' => Yii::t('backend', 'Select cinema hall')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
        <div class="col">
            <?=
            $form->field($model, 'cinema_film_id')->widget(Select2::className(), [
                'data' => $films,
                'options' => ['multiple' => false, 'placeholder' => Yii::t('backend', 'Select cinema film')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'places')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col">
            <?=
            $form->field($model, 'show_time')->widget(DateTimePicker::className(), [
                'options' => ['placeholder' => 'Укажите дату и время сеанса'],
                'pickerButton' => ['icon' => 'time'],
                'convertFormat' => true,
                'language' => 'ru',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'php:Y-m-d H:i:00',
                    'todayHighlight' => false,
                    'startDate' => date('Y-m-d H:i:00')
                ]
            ])
            ?>
        </div>
        <div class="col">
            <?=
            $form->field($model, 'status')->widget(Select2::className(), [
                'data' => ['Bay' => 'Bay', 'Armor' => 'Armor'],
                'options' => ['multiple' => false, 'placeholder' => Yii::t('backend', 'Select status')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ?>
            <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
