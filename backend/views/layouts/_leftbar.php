<?php

use yii\bootstrap\Html;
?>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="/backend/web/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"></div>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php // echo Yii::$app->user->identity->profile['name']         ?> <span class="caret"></span></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="#"><i class="ti-user"></i> <?= Yii::t('backend', 'My Profile') ?></a></li>
                    <li><a href="#"><i class="ti-email"></i> <?= Yii::t('backend', 'Inbox') ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#"><i class="ti-settings"></i> <?= Yii::t('backend', 'Account Setting') ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <?= Html::beginForm(['/user/security/logout'], 'post', ['id' => 'logout']) ?>
                        <a href="javascript:;" onclick="document.getElementById('logout').submit();"><i class="fa fa-power-off"></i> <?= Yii::t('backend', 'Logout') ?></a>
                        <?= Html::endForm() ?>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                    </span> </div>
                <!-- /input-group -->
            </li>
            <li class="nav-small-cap m-t-10">&nbsp;&nbsp; <?= Yii::t('backend', 'Основные данные') ?></li>
            <?php if (Yii::$app->userData->isAdmin()) { ?>
                <li><a href="javascript:void(0);" class="waves-effect"><i data-icon=")" class="fa fa-magic fa-fw"></i> <span class="hide-menu"><?= Yii::t('backend', 'Cinema') ?><span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/cinema/index"><?= Yii::t('backend', 'Cinema') ?></a></li>
                        <li><a href="/cinema/films"><?= Yii::t('backend', 'Films') ?></a></li>
                        <li><a href="/cinema/halls"><?= Yii::t('backend', 'Halls') ?></a></li>
                    </ul>
                </li>
                <li><a href="/orders/index" class="waves-effect"><i data-icon=")" class="fa fa-money fa-fw"></i> <span class="hide-menu"><?= Yii::t('backend', 'Orders') ?></a></li>
                <li class="nav-small-cap m-t-10">&nbsp;&nbsp; <?= Yii::t('backend', 'Пользователи') ?></li>
                <li><a href="javascript:void(0);" class="waves-effect"><i data-icon=")" class="icon-people fa-fw"></i> <span class="hide-menu"><?= Yii::t('user', 'Users') ?><span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/user/admin/index"><?= Yii::t('backend', 'Список пользователей') ?></a></li>
                        <li><a href="/rbac/role/index"><?= Yii::t('user', 'Роли') ?></a></li>
                        <li><a href="/rbac/permission/index"><?= Yii::t('user', 'Права') ?></a></li>
                        <li><a href="/rbac/rule/index"><?= Yii::t('user', 'Правила') ?></a></li>
                        <li><a href="/auth/history/logs"><?= Yii::t('backend', 'Архив авторизаций') ?></a></li>
                        <li><a href="javascript:void(0)" class="waves-effect"><?= Yii::t('user', 'Создать') ?>
                                <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="/user/admin/create"><?= Yii::t('user', 'Пользователя') ?></a></li>
                                <li><a href="/rbac/role/create"><?= Yii::t('user', 'Роль') ?></a></li>
                                <li><a href="/rbac/permission/create"><?= Yii::t('user', 'Права') ?></a></li>
                                <li><a href="/rbac/rule/create"><?= Yii::t('user', 'Правила') ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>

