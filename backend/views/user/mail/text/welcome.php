<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var dektrium\user\models\User
 */
?>
<?= Yii::t('user', 'Hello') ?>,

<?= Yii::t('user', 'Your <a href="https://sota.football/history" target="_blank">Sota</a> has been created.') ?>.
<?php if ($module->enableGeneratingPassword): ?>
<?= Yii::t('user', 'Here is your password') ?>:
<?= $user->password ?>
<?php endif ?>
