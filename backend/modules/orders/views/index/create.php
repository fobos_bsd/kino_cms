<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderTicket */

$this->title = Yii::t('backend', 'Create Order Ticket');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Order Tickets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-orange">
            <div class="panel-heading"> <?= Html::encode($this->title) ?></div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <?=
                $this->render('_form', [
                    'model' => $model,
                    'halls' => $halls,
                    'films' => $films
                ])
                ?>
            </div>
        </div>
    </div>
</div>
