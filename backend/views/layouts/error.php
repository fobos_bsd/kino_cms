<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use backend\assets\AppAsset,
    backend\assets\BackendAsset;
use common\widgets\Alert;

AppAsset::register($this);
BackendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <section id="wrapper" class="error-page">
            <div class="error-box">
                <div class="error-body text-center">
                    <?= Alert::widget() ?>
                    <?= $content ?>
                    <footer class="footer text-center"><?= date('Y') ?> &copy; zapchasty.com</footer>
                </div>
        </section>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
