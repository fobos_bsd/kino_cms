<?php

namespace backend\modules\auth\models;

use Yii;
use yii\base\Model;

/**
 * Description of JobHistoryLog
 * Add log for Job History Modifide
 * @author fobos
 */
class HistoryLog extends Model {

    // Get log`s list from folder for job history
    final public function getListLog(): array {
        // Set folder for logs
        $log_path = Yii::getAlias('@backend') . '/web/uploads/auth_history';

        // Arr of files
        $files = [];

        // Add headers for columns
        if (file_exists($log_path)) {
            $list = scandir($log_path);

            // Check is not dir and system folder
            foreach ($list as $item) {
                $file = $log_path . '/' . $item;

                if ($item !== '.' && $item !== '..' && !is_dir($file)) {
                    array_push($files, $item);
                }
            }
        }

        return $files;
    }

}
