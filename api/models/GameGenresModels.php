<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\caching\TagDependency;
use common\models\GameGenreTranslate;

/**
 * Description of GameGenresModels
 *
 * @author fobos
 */
class GameGenresModels extends Model {

    // Get data for GameRoles
    final public function getTranslate($get) {

        $lang = $get['lang'] ?? 'ru';

        // Get data with cache in redis
        $data = GameGenreTranslate::getDb()->cache(function() use ($lang) {
            $query = GameGenreTranslate::find()->select(['game_genre_id', 'title'])->where(['language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))])->asArray()->all();
            return $query;
        }, 3600, new TagDependency(['tags' => 'cache_gamegenre_translate']));

        $result['error'] = '';
        $result['content'] = $data;

        return $result;
    }

}
