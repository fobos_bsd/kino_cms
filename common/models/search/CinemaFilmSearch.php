<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CinemaFilm;

/**
 * CinemaFilmSearch represents the model behind the search form of `common\models\CinemaFilm`.
 */
class CinemaFilmSearch extends CinemaFilm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cinema_id'], 'integer'],
            [['name', 'show_start_date', 'show_end_date', 'show_times'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CinemaFilm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cinema_id' => $this->cinema_id,
            'show_start_date' => $this->show_start_date,
            'show_end_date' => $this->show_end_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'show_times', $this->show_times]);

        return $dataProvider;
    }
}
