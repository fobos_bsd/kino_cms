<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\data\Pagination;
use common\models\Events;

/**
 * Description of EventsModels
 *
 * @author fobos
 */
class EventsModels extends Model {

    // Get list all data of events for the group
    final public function getEventsList($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;
        $title = $get['title'] ?? null;
        $content = $get['content'] ?? null;
        $clan_id = $get['clan_id'] ?? null;

        //Check is user in clan
        if (isset($clan_id) && Yii::$app->acl->isInClan($clan_id) === false) {
            $clan_id = null;
        }

        // Get data
        $query = Events::find()
                ->orderBy(['created_date' => SORT_DESC, 'title' => SORT_ASC]);

        // Filter
        if (isset($title)) {
            $query->andWhere(['ilike', 'title', strip_tags(trim($title))]);
        }
        if (isset($content)) {
            $query->andWhere(['ilike', 'content', strip_tags(trim($content))]);
        }
        if (isset($clan_id)) {
            $query->andWhere(['clan_id' => (int) $clan_id]);
        } else {
            $query->andWhere(['is', 'clan_id', null]);
        }

        // Set pagination data
        $countQuery = clone $query;
        $tottal_pages = (int) $countQuery->count();
        $pages = new Pagination(['totalCount' => $tottal_pages]);
        $pages->defaultPageSize = $count_per_page;
        $pages->page = $page;
        $contents = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->asArray()
                ->all();

        // Set results
        $result['error'] = '';
        $result['content'] = $contents;
        $result['count_pages'] = ((int) $tottal_pages > 0) ? ceil((int) $tottal_pages / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Get data for one Event of Group
    final public function getGroupEvent($get) {

        $itemID = (int) $get['id'];

        // Get data
        $content = Events::find()
                        ->joinWith(['author' => function($q) {
                                $q->select(['user.id', 'user.username', 'user.email', 'profile.user_id', 'profile.name', 'profile.avatar', 'profile.gender']);
                                $q->leftJoin('profile', '"profile"."user_id" = "user"."id"');
                            }])
                        ->where(['events.id' => $itemID])
                        ->asArray()->one();

        if (isset($content['clan_id']) && !empty($content['clan_id']) && Yii::$app->acl->isInClan($content['clan_id']) === false) {
            $content = [];
        }

        $result['error'] = '';
        $result['content'] = $content;

        return $result;
    }

}
