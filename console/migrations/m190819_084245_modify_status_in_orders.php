<?php

use yii\db\Migration;

/**
 * Class m190819_084245_modify_status_in_orders
 */
class m190819_084245_modify_status_in_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order_ticket', 'status', "ENUM('Bay', 'Armor', 'In process') NOT NULL DEFAULT 'Bay'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190819_084245_modify_status_in_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190819_084245_modify_status_in_orders cannot be reverted.\n";

        return false;
    }
    */
}
