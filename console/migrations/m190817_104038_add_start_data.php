<?php

use yii\db\Migration;

/**
 * Class m190817_104038_add_start_data
 */
class m190817_104038_add_start_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cinema_film', 'price', $this->decimal(4,2)->notNull()->defaultValue(0.0));
        
        $this->insert('cinema', [
            'name' => 'ім.Шевченко'
        ]);
        $this->insert('cinema_hall', [
            'cinema_id' => 1,
            'name' => '№1',
            'places' => '{"r1":[1,2,3,4,5,6,7,8,9,10,11,12],"r2":[1,2,3,4,5,6,7,8,9,10,11,12,13,14],"r3":[1,2,3,4,5,6,7,8,9,10,11,12,13,15],'
            . '"r4":[1,2,3,4,5,6,7,8,9,10,11,12,13],"r5":[1,2,3,4,5,6,7,8,9,10,11,12,13],"r6":[1,2,3,4,5,6,7,8,9,10,11,12,13],"r7":[1,2,3,4,5,6,7,8,9,10,11,12,13],"r8":[1,2,3,4,5,6,7,8,9,10,11,12,13],'
            . '"r9":[1,2,3,4,5,6,7,8,9,10,11,12,13],"r4":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]}'
        ]);
        $this->insert('cinema_film', [
            'cinema_id' => 1,
            'name' => 'Планета Ка-Пекс',
            'show_start_date' => '2019-08-16',
            'show_end_date' => '2019-08-25',
            'show_times' => '["10:00","12:00","16:00","19:00"]',
            'price' => 50.0
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190817_104038_add_start_data cannot be reverted.\n";
        $this->delete('cinema_film', ['id' => 1]);
        $this->delete('cinema_hall', ['id' => 1]);
        $this->delete('cinema', ['id' => 1]);
        
        $this->dropColumn('cinema_film', 'price');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190817_104038_add_start_data cannot be reverted.\n";

        return false;
    }
    */
}
