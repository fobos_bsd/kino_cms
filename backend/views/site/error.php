<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\bootstrap\Html;

$this->title = $name;
?>

<section id="wrapper" class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <?php if (isset($exception->statusCode)) { ?>
                <?php if ($exception->statusCode == 404) { ?>
                    <h1><?= $exception->statusCode ?></h1>
                    <h3 class="text-uppercase"><?= Yii::t('backend', 'Страница не найдена') ?> !</h3>
                    <p class="text-muted m-t-30 m-b-30 text-uppercase">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
                <?php } elseif ($exception->statusCode == 400) { ?>
                    <h1><?= $exception->statusCode ?></h1>
                    <h3 class="text-uppercase"><?= Yii::t('backend', 'Страница не найдена') ?> !</h3>
                    <p class="text-muted m-t-30 m-b-30 text-uppercase">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
                <?php } elseif ($exception->statusCode == 403) { ?>
                    <h1><?= $exception->statusCode ?></h1>
                    <h3 class="text-uppercase"><?= Yii::t('backend', 'Ошибка доступа') ?></h3>
                    <p class="text-muted m-t-30 m-b-30 text-uppercase"><?= Yii::t('backend', 'У Вас нет прав для просмотра данной страницы') ?>.</p>
                <?php } elseif ($exception->statusCode == 500) { ?>
                    <h1><?= $exception->statusCode ?></h1>
                    <h3 class="text-uppercase"><?= Yii::t('backend', 'Внутренняя ошибка сервера') ?>.</h3>
                    <p class="text-muted m-t-30 m-b-30 text-uppercase"><?= Yii::t('backend', 'Пожалуйста, повторите поппытку позже') ?></p>
                <?php } elseif ($exception->statusCode == 503) { ?>
                    <h1><?= $exception->statusCode ?></h1>
                    <h3 class="text-uppercase"><?= Yii::t('backend', 'Данный сайт скоро будет доступен') ?>.</h3>
                    <p class="text-muted m-t-30 m-b-30 text-uppercase"><?= Yii::t('backend', 'Пожалуйста, повторите поппытку позже') ?></p>
                <?php } else { ?>
                    <h1><?= Html::encode($exception->statusCode) ?></h1>
                    <p class="text-muted m-t-30 m-b-30 text-uppercase"><?= nl2br(Html::encode($message)) ?></p>
                <?php } ?>
            <?php } ?>
        </div>
</section>

