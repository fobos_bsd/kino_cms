<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cinema_hall".
 *
 * @property int $id
 * @property int $cinema_id
 * @property string $name
 * @property string $places
 *
 * @property Cinema $cinema
 * @property OrderTicket[] $orderTickets
 */
class CinemaHall extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'cinema_hall';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['cinema_id', 'name', 'places'], 'required'],
            [['cinema_id'], 'integer'],
            [['places'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['cinema_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cinema::className(), 'targetAttribute' => ['cinema_id' => 'id']],
            [['name', 'places'], 'filter', 'filter' => function($value) {
                    return strip_tags(trim($value));
                }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common', 'ID'),
            'cinema_id' => Yii::t('common', 'Cinema ID'),
            'name' => Yii::t('common', 'Name'),
            'places' => Yii::t('common', 'Places'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCinema() {
        return $this->hasOne(Cinema::className(), ['id' => 'cinema_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTickets() {
        return $this->hasMany(OrderTicket::className(), ['cinema_hall_id' => 'id']);
    }

}
