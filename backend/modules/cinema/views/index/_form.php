<?php

use yii\bootstrap4\Html,
    yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cinema */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel-body">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
