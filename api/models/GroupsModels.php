<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api\models;

use Yii,
    yii\base\Model,
    yii\data\Pagination,
    yii\caching\TagDependency,
    yii\web\UploadedFile;
use common\models\Clan,
    common\models\ClanUsers,
    common\models\ClanStatusRulesRelation,
    common\models\UserGames;

/**
 * Description of GroupsModels
 *
 * @author fobos
 */
class GroupsModels extends Model {

    // Get list all data
    final public function getGroupsList($get) {

        // Set start data
        $page = (isset($get['page'])) ? (int) $get['page'] - 1 : 0;
        $count_per_page = (isset($get['count_par_pare'])) ? (int) $get['count_par_pare'] : 20;
        $active = $get['active'] ?? null;
        $name = $get['name'] ?? null;
        $desc = $get['desc'] ?? null;

        // Get data
        $contents = Clan::getDb()->cache(function() use ($count_per_page, $page, $active, $name, $desc) {
            $query = Clan::find()
                    ->orderBy(['name' => SORT_ASC, 'game_id' => SORT_DESC]);

            // Filter
            if (isset($active)) {
                $query->where(['active' => strip_tags(trim($active))]);
            }
            if (isset($name)) {
                $query->andFilterWhere(['like', 'name', strip_tags(trim($name))]);
            }
            if (isset($desc)) {
                $query->andFilterWhere(['like', 'description', strip_tags(trim($desc))]);
            }

            // Set pagination data
            $countQuery = clone $query;
            $tottal_pages = (int) $countQuery->count();
            $pages = new Pagination(['totalCount' => $tottal_pages]);
            $pages->defaultPageSize = $count_per_page;
            $pages->page = $page;
            $contents = $query->offset($pages->offset)
                    ->limit($pages->limit)
                    ->asArray()
                    ->all();
            return ['contents' => $contents, 'tottal_pages' => $tottal_pages];
        }, 3600, new TagDependency(['tags' => 'cache_clan_api']));

        // Set results
        $result['error'] = '';
        $result['content'] = $contents['contents'];
        $result['count_pages'] = ((int) $contents['tottal_pages'] > 0) ? ceil((int) $contents['tottal_pages'] / (int) $count_per_page) : 0;
        $result['per_page'] = $count_per_page;

        return $result;
    }

    // Get data for one Group
    final public function getGroupData($get) {

        $itemID = (int) $get['id'];
        $lang = $get['lang'] ?? 'ru';

        // Count roles in clan
        $count_roles = ClanUsers::find()->select(['clan_users_roles.clan_roles_id'])->leftJoin('clan_users_roles', 'clan_users_roles.clan_users_id = clan_users.id')->where(['clan_users.clan_id' => $itemID])->asArray()->all();

        // Get data
        $query = Clan::find()
                ->joinWith(['clanUsers' => function($q)use($lang, $count_roles) {
                        $q->select(['clan_users.id', 'clan_users.active_status', 'clan_users.user_status', 'clan_users.start_date', 'clan_users.clan_id', 'clan_users.user_id', 'clan_users.clan_group_id']);
                        $q->andWhere(['clan_users.active_status' => 'active']);
                        $q->joinWith(['user' => function($sq) {
                                $sq->select(['user.id', 'user.username', 'user.email', 'profile.avatar as avatar']);
                                $sq->leftJoin('profile', '"profile"."user_id" = "user"."id"');
                            }]);
                        $q->joinWith(['clanUsersRoles' => function($sq)use($lang, $count_roles) {
                                $sq->select(['roles_ingame.name', 'roles_ingame.id', 'roles_ingame.icons', 'clan_users_roles.mane_role', 'clan_users_roles.clan_users_id', 'roles_ingame_translate.name as title']);
                                $sq->leftJoin('roles_ingame', 'clan_users_roles.clan_roles_id = roles_ingame.id');
                                $sq->leftJoin('roles_ingame_translate', 'roles_ingame_translate.roles_ingame_id = roles_ingame.id');
                                if (isset($count_roles[0]['clan_roles_id'])) {
                                    $sq->andFilterWhere(['roles_ingame_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);
                                }
                            }]);
                        $q->orderBy(['clan_users.user_status' => SORT_ASC]);
                    }])
                ->where(['clan.id' => $itemID, 'clan.active' => true]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

    // Get data for Quin of the Group
    final public function getGroupQuinData($get) {

        $itemID = (int) $get['id'];
        $lang = $get['lang'] ?? 'ru';

        // Get data
        $query = Clan::find()
                ->joinWith(['clanUsers' => function($q)use($lang) {
                        $q->select(['clan_users.id', 'clan_users.active_status', 'clan_users.user_status', 'clan_users.start_date', 'clan_users.clan_id', 'clan_users.user_id']);
                        $q->andWhere(['clan_users.active_status' => 'active']);
                        $q->andWhere(['clan_users.user_status' => 1]);
                        $q->joinWith(['user' => function($sq) {
                                $sq->select(['user.id', 'user.username', 'user.email', 'profile.avatar as avatar']);
                                $sq->leftJoin('profile', '"profile"."user_id" = "user"."id"');
                            }]);
                        $q->joinWith(['clanUsersRoles' => function($sq)use($lang) {
                                $sq->select(['roles_ingame.name', 'roles_ingame.id', 'roles_ingame.icons', 'clan_users_roles.mane_role', 'clan_users_roles.clan_users_id', 'roles_ingame_translate.name as title']);
                                $sq->leftJoin('roles_ingame', 'clan_users_roles.clan_roles_id = roles_ingame.id');
                                $sq->leftJoin('roles_ingame_translate', 'roles_ingame_translate.roles_ingame_id = roles_ingame.id');
                                $sq->where(['roles_ingame_translate.language_id' => Yii::$app->userData->getLangID(strip_tags(trim($lang)))]);
                            }]);
                    }])
                ->where(['clan.id' => $itemID, 'clan.active' => true]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->one();

        return $result;
    }

    // Get users list who request to enter to Clan
    final public function getRequestedUsers($get) {

        $itemID = (int) $get['id'];

        // Get data
        $query = ClanUsers::find()
                        ->joinWith(['user' => function($q) {
                                $sq->select(['user.id', 'user.username', 'user.email', 'profile.avatar as avatar']);
                                $sq->leftJoin('profile', '"profile"."user_id" = "user"."id"');
                            }])
                        ->where(['clan_users.clan_id' => $itemID, 'clan_users.active_status' => 'no confirmed', 'clan_users.who_confirmed' => null, 'clan_users.who_invited' => null])->andWhere(['>=', 'clan_users.modification_date', date('Y-m-01')]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Get users list whom invited to Clan
    final public function getInvitedUsers($get) {

        $itemID = (int) $get['id'];

        // Get data
        $query = ClanUsers::find()
                        ->joinWith(['user' => function($q) {
                                $sq->select(['user.id', 'user.username', 'user.email', 'profile.avatar as avatar']);
                                $sq->leftJoin('profile', '"profile"."user_id" = "user"."id"');
                            }])
                        ->where(['clan_users.clan_id' => $itemID, 'clan_users.active_status' => 'no confirmed'])->andWhere(['>=', 'clan_users.modification_date', date('Y-m-01')])->andWhere(['is not', 'clan_users.who_invited', null]);

        $result['error'] = '';
        $result['content'] = $query->asArray()->all();

        return $result;
    }

    // Add clan status rule data
    final public function setClanStatusRule($data) {

        $result['status'] = 'success';
        $result['error'] = '';

        $model = new ClanStatusRulesRelation();
        $model->clan_status_list_id = $data['clan_status_id'];
        $model->clan_status_rules_id = $data['clan_rules_id'];
        $model->clan_id = $data['clan_id'];
        $model->rules_value = $data['rules_value'];
        if (!$model->save()) {
            $result['status'] = 'error';
            $result['error'] = (array) $model->getErrors();
        }

        return $result;
    }

    // Create clan data with check acl
    final public function createClan($data) {
        $result['error'] = '';
        $result['status'] = 'success';

        //Check duble clan for one game
        if (isset($data['game_id']) && $this->checkDubleGameClan($data['game_id']) === true) {
            $result['status'] = 'error';
            $result['error'] = 'You are already has clan in the thame game!';

            return $result;
        }

        // Check is user has personage in the game
        if ($this->hasPersonazeInGame($data['game_id']) === false) {
            $result['status'] = 'error';
            $result['error'] = 'You are not have person in the thame game!';

            return $result;
        }

        $model = new Clan();
        $model->loadDefaultValues();
        $model->name = $data['name'];
        $model->active = true;
        $model->description = $data['description'] ?? null;
        $model->game_id = $data['game_id'] ?? null;
        $model->logo = UploadedFile::getInstance($model, 'logo') ?? null;
        $model->users_wate = $data['users_wate'] ?? 1;
        $model->oner_id = Yii::$app->user->id;

        if (!$model->save()) {
            Yii::$app->response->statusCode = 500;
            $result['status'] = 'error';
            $result['error'] = (array) $model->getErrors();
        }

        $clanID = $model->id;
        $result['clan_id'] = $clanID;

        // Add autor as clan quin
        $model_users = new ClanUsers();
        $model_users->clan_id = $clanID;
        $model_users->user_id = Yii::$app->user->id;
        $model_users->clan_group_id = null;
        $model_users->parent_user_id = null;
        $model_users->active_status = 'active';
        $model_users->user_status = 1;
        $model_users->start_date = date('Y-m-d');
        $model_users->modification_date = $model_users->start_date;
        $model_users->confirmed_key = 'nokey';
        $model_users->confirmed_key = null;

        if (!$model_users->save()) {
            $result['status'] = 'error';
            $result['error'] = (array) $model->getErrors();
        }

        return $result;
    }

    // Update clan data with check acl
    final public function updateClan($data) {
        $result['error'] = '';
        $result['status'] = 'success';
        $groupID = (int) $data['id'];

        $model = Clan::findOne(['id' => $groupID, 'oner_id' => Yii::$app->user->id]);
        $model->scenario = 'update';

        //Check duble clan for one game
        if (isset($data['game_id']) && $model->game_id !== $data['game_id'] && $this->checkDubleGameClan($data['game_id']) === true) {
            $result['status'] = 'error';
            $result['error'] = 'You are already has clan in the thame game!';

            return $result;
        }

        $model->name = $data['name'] ?? $model->name;
        $model->active = $data['active'] ?? $model->active;
        $model->description = $data['description'] ?? $model->description;

        // Check is user has personage in the game and is alown in clan
        if ($this->countUsersInClan($groupID) == 1 && $this->hasPersonazeInGame($data['game_id']) === true && isset($data['game_id'])) {
            $model->game_id = $data['game_id'];
        }

        if (isset($data['logo'])) {
            $model->logo = UploadedFile::getInstanceByName($data['logo']);
            $model->logo = $model->upload();
        }
        $model->users_wate = $data['users_wate'] ?? $model->users_wate;

        if (!$model->save()) {
            $result['status'] = 'error';
            $result['error'] = (array) $model->getErrors();
        }

        return $result;
    }

    // Delete Clan
    final public function deleteClan($data) {
        $result['error'] = '';
        $result['status'] = 'success';
        $groupID = (int) $data['id'];

        //Check is clan is empty
        if ($this->countUsersInClan($groupID) < 2) {
            $model = Clan::findOne(['id' => $groupID, 'oner_id' => Yii::$app->user->id]);
            $model->delete();
        } else {
            $result['error'] = 'This is clan is not empty! You cal only change oner or delete all users from the clan, than remove the clan.';
            $result['status'] = 'error';
        }

        return $result;
    }

    // Update clan logo
    final public function updateAvatar(int $clan_id, $logo) {

        // Set results
        $result['error'] = '';
        $result['status'] = 'success';

        if (($model = Clan::findOne(['id' => $clan_id, 'oner_id' => Yii::$app->user->id])) !== null) {
            $model->scenario = 'update';
            $model->logo = UploadedFile::getInstanceByName('logo');
            $model->logo = $model->upload();
            if (!$model->save()) {
                $result['error'] = (array) $model->getErrors();
                $result['status'] = 'error';
            }
        } else {
            $result['error'] = 'User with this ID was not found';
            $result['status'] = 'error';
        }

        return $result;
    }

    //Check if user has clan for the thame game
    final private function checkDubleGameClan(int $game_id): bool {
        $result = true;

        if (Clan::findOne(['oner_id' => Yii::$app->user->id, 'game_id' => $game_id]) === null) {
            $result = false;
        }

        return $result;
    }

    // If user has personaz in game
    final private function hasPersonazeInGame(int $game_id): bool {
        $result = false;

        if (UserGames::findOne(['user_id' => Yii::$app->user->id, 'games_id' => $game_id]) !== null) {
            $result = true;
        }

        return $result;
    }

    // Count users in the clan
    final private function countUsersInClan(int $clan_id): int {
        return ClanUsers::find()->where(['clan_id' => $clan_id])->count();
    }

}
