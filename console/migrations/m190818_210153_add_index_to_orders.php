<?php

use yii\db\Migration;

/**
 * Class m190818_210153_add_index_to_orders
 */
class m190818_210153_add_index_to_orders extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createIndex('fk_order_ticket_client_name_idx', 'order_ticket', 'client_name');
        $this->createIndex('fk_order_ticket_status_idx', 'order_ticket', 'status');
        $this->createIndex('fk_order_ticket_show_time_idx', 'order_ticket', 'show_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m190818_210153_add_index_to_orders cannot be reverted.\n";

        $this->dropIndex('fk_order_ticket_show_time_idx', 'order_ticket');
        $this->dropIndex('fk_order_ticket_status_idx', 'order_ticket');
        $this->dropIndex('fk_order_ticket_client_name_idx', 'order_ticket');

        return false;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190818_210153_add_index_to_orders cannot be reverted.\n";

      return false;
      }
     */
}
