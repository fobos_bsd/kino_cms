<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */
$this->title = Yii::t('user', 'Sign in');
?>

<div class="container-fluid">
    <div id="page-login" class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="text-right">
                <?php if ($module->enableConfirmation): ?>
                    <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend'], ['class' => 'txt-default']) ?>
                <?php endif ?>
                <?php if ($module->enableRegistration): ?>
                    <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register'], ['class' => 'txt-default']) ?>
                <?php endif ?>
            </div>
            <div class="box">
                <div class="box-content">
                    <div class="text-center">
                        <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                    </div>
                    <?php
                    $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                                'validateOnBlur' => false,
                                'validateOnType' => false,
                                'validateOnChange' => false,
                            ])
                    ?>
                    <div class="form-group">
                        <label class="control-label">Username</label>
                        <?=
                        $form->field(
                                $model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
                        )
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <?=
                                $form
                                ->field(
                                        $model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']]
                                )
                                ->passwordInput()
                                ->label(
                                        Yii::t('user', 'Password')
                                        . ($module->enablePasswordRecovery ?
                                                ' (' . Html::a(
                                                        Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5']
                                                )
                                                . ')' : '')
                                )
                        ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>
                    </div>
                    <div class="text-center">
                        <?=
                        Html::submitButton(
                                Yii::t('user', 'Sign in'), ['class' => 'btn btn-primary']
                        )
                        ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'login-form',
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                            'validateOnBlur' => false,
                            'validateOnType' => false,
                            'validateOnChange' => false,
                        ])
                ?>

                <?=
                $form->field(
                        $model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
                )
                ?>

                <?=
                        $form
                        ->field(
                                $model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']]
                        )
                        ->passwordInput()
                        ->label(
                                Yii::t('user', 'Password')
                                . ($module->enablePasswordRecovery ?
                                        ' (' . Html::a(
                                                Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5']
                                        )
                                        . ')' : '')
                        )
                ?>

                <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>

                <?=
                Html::submitButton(
                        Yii::t('user', 'Sign in'), ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']
                )
                ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
            </p>
        <?php endif ?>
        <?php if ($module->enableRegistration): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
            </p>
        <?php endif ?>
        <?=
        Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ])
        ?>
    </div>
</div>
